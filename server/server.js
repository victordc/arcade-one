var path = require('path')
var express = require('express')


module.exports = {
  app: function () {
    var app = express()
    var publicPath = express.static(path.join(__dirname, '../'))
    app.use('/', publicPath)
    return app
  }
}