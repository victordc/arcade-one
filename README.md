# ![WebApp](logo.png)



# WebApp
A WebApp for Admin, Arcade-One.


## Site
Under Construction.

## Mobile support
The WebApp is usable on devices of all sizes, and improvements are constantly being made in this area.


### Development

To fix a bug or enhance an existing module, follow these steps:

- Fork the repo
- Create a new branch (`git checkout -b name(ticket no)`) example: victor6668
- $ npm install
- $ npm start
- Commit your changes (`git commit -am victor6668`)
- Push to the branch (`git push origin victor6668`)



## Built with

- [Angular 2] (https://angular.io/) - Angular is a development platform for building mobile and desktop web applications.
- [Angular material] (https://material.angular.io/) - Hit the ground running with comprehensive, modern UI components that work across the web, mobile and desktop.
- [Bootstrap] (http://getbootstrap.com/) - Extensive list of components and  Bundled Javascript plugins.
- [TypeScript] (http://www.typescriptlang.org/) - Javascript that scales



© Diet Code