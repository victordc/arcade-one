import { Component, OnInit } from '@angular/core';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Injectable, Inject } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router'
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

@Component({
	moduleId:module.id,
  	selector: 'my-app',
  	providers:[NotificationsService,Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  	templateUrl: 'layout.component.html'

})

export class ArcadeOneComponent {
	location:Location;
	navLinks:Array<any>;
	sideHide:any = 0;
	options:Object={
		position: ["top", "right"],
    	timeOut: 5000,
	        showProgressBar: false,
	        pauseOnHover: false,
	        clickToClose: true,
	        maxLength: 100,
    	lastOnBottom: true,
		preventDuplicates:true
    };
	constructor(private notif:NotificationsService,  location:Location, router:Router){
		this.location = location;
		router.events.subscribe((event) => {
			let x  = this.location.path();
			if(x == '/login' || x == '/forgot' || x == '' ){
				this.sideHide=1;
			}
			else if( /change-password/.test(x)){
				this.sideHide=1;
			}
			else{
				this.sideHide=0;
			}
		});
	}

	ngOnInit() {
		let x  = this.location.path();
		if(x == '/login' || x == '/forgot' || x == '' ){
			this.sideHide=1;
		}
		else if( /change-password/.test(x)){
			this.sideHide=1;
		}
		else{
			this.sideHide=0;
		}
		// console.log(this.sideHide,x.indexOf('/change-password'));
		//nav links
	    // this.navLinks = ['Home','Campaigns','Brands','Influencers','Conflicts','Payments','Admin Users','Logout'];
	    this.navLinks = [{'name': 'Home', 'route': 'dashboard'},
	    				 {'name': 'Campaigns', 'route': 'campaigns'},
	    				 {'name': 'Brands', 'route': 'brands'},
	    				 {'name': 'Influencers', 'route': 'influencers'},
	    				 // {'link': 'Conflicts', 'route': 'conflicts'},
	    				  {'name': 'Payments', 'route': 'payments'},
	    				 {'name': 'Admin Users', 'route': 'users'},
	    				 {'name': 'Logout', 'route': 'logout'}];
	}

	success(msg:any){
		this.notif.success(
	    (msg && msg.title) || msg ,
	    (msg && msg.details),
	    {
	        timeOut: 5000,
	        showProgressBar: true,
	        pauseOnHover: false,
	        clickToClose: false,
	        maxLength: 10
	    }
	)
	}

}
