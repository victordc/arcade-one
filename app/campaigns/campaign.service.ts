import { Http, Response, Headers, RequestOptions } from '@angular/http'
import {Observable} from 'rxjs/Rx';
import { AuthHttp } from '../secure-http.service';

import {Injectable,Inject} from '@angular/core';
@Injectable()
export class CampaignService {

	constructor(private authHttp:AuthHttp){

	}

	private baseURL = 'http://instatise-server-dev.herokuapp.com/api';

	getCampaignsList(sort:any,page:number, value:string):Observable<any>{
		return this.authHttp.get(`${this.baseURL}/admin/listcampaigns?total=10&page=${page}&sort=${sort}&search_key=${value}`)
				.map(resp=>resp.json())
	}

	getCampaignDetails(id:number):Observable<any>{
		return this.authHttp.get(`${this.baseURL}/campaigndetails?campaign_id=${id}`)
				.map(resp=>resp.json())
	}

	getCategories():Observable<any>{
		return this.authHttp.get(`${this.baseURL}/listcategories`)
				.map(resp=>resp.json())
	}

	getProposals(id:number):Observable<any>{
		return this.authHttp.get(`${this.baseURL}/getproposals?campaign_id=${id}&sort=dsccrt&total=10&page=1&influencer_id=`)
				.map(resp=>resp.json())
	}

	updateCampaign(campaign:Object){
		return this.authHttp.post(`${this.baseURL}/updatecampaign`,campaign);
	}
}
