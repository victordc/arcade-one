"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var campaign_service_component_1 = require('./service/campaign-service.component');
var http_1 = require('@angular/http');
var angular2_notifications_1 = require('angular2-notifications');
var CryptoJS = require('crypto-js');
var CampaignComponent = (function () {
    function CampaignComponent(location, http, notif, campaignService) {
        this.location = location;
        this.http = http;
        this.notif = notif;
        this.campaignService = campaignService;
        this.campaignStatus = 'loading';
        this.campaignId = 0;
        this.cloudinaryAPISecret = 'zwZBoDciK30vckcAlhcYV2kodSU';
    }
    CampaignComponent.prototype.ngOnInit = function () {
        // console.log(location.pathname);
        this.campaignId = +location.pathname.split('/')[2];
        this.getCategoriesList();
        this.getProposalsList();
        this.getDataFromServer();
    };
    CampaignComponent.prototype.getCategoriesList = function () {
        var _this = this;
        this.campaignService.getCategories()
            .subscribe(function (data) {
            // Succesfully fetched data
            if (data.code === 200) {
                _this.categories = data.data;
            }
            else {
                alert("Error: " + data.code);
            }
        }, function (error) { }, // in case of failure show this message
        function () { } //run this code in all cases
        );
    };
    CampaignComponent.prototype.getProposalsList = function () {
        var _this = this;
        this.campaignService.getProposals(this.campaignId)
            .subscribe(function (data) {
            // Succesfully fetched data
            if (data.code === 200) {
                _this.proposalsExist = true;
                _this.proposals = data.data;
            }
            else {
                // console.log('No Proposals');
                _this.proposalsExist = false;
            }
        }, function (error) { }, // in case of failure show this message
        function () { } //run this code in all cases
        );
    };
    CampaignComponent.prototype.updateAndUploadImageToCloud = function (fileInput, campaign) {
        var _this = this;
        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var uploadData = {
                    timestamp: +new Date(),
                    api_key: '116931532498815',
                    file: e.target.result,
                    signature: CryptoJS.SHA1("timestamp=" + +new Date() + _this.cloudinaryAPISecret).toString()
                };
                _this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload', uploadData).subscribe(function (resp) {
                    campaign.campaign_image = resp.json().secure_url;
                });
            };
            reader.readAsDataURL(fileInput.target.files[0]);
        }
        ;
    };
    CampaignComponent.prototype.updateCampaign = function (campaign) {
        // console.log(campaign);
        campaign.campaign_id = this.campaign.id;
        // console.log(campaign);
        this.campaignService.updateCampaign(campaign)
            .subscribe(function (resp) {
            if (resp.json().code === 200) {
                // console.log(resp);
                alert('Success');
            }
            else {
                // console.log(resp);
                alert('Failure');
            }
        });
    };
    // disableOrEnableCampaign(campaign:any){
    //     campaign.active = !campaign.active;
    //     campaign.campaign_id = this.campaign.id;
    //     this.campaignService.updateCampaign(campaign).subscribe((data) => {
    //         if(data.code === 200 ){
    //             this.campaignService.getCampaignDetails(data.id).subscribe((data) =>{
    //                 this.campaign = data;
    //                 this.enableOrDisable = data.active ? 'Disable' : 'Enable';
    //                 this.enableOrDisableMsg = data.active ? ' Campaign Enabled' : 'Campaign Disabled';
    //                 this.notif.success('Success!!',this.enableOrDisableMsg);//sucess //heading //description
    //             });
    //         }else{
    //             console.log(data);
    //             this.notif.error('Failure!!',"");
    //         }
    //     })
    // };
    CampaignComponent.prototype.getDataFromServer = function () {
        var _this = this;
        this.campaignService.getCampaignDetails(this.campaignId)
            .subscribe(function (data) {
            // Succesfully fetched data
            if (data.code === 200) {
                _this.campaign = data;
                // console.log(this.campaign);
                _this.enableOrDisable = data.active ? 'Disable' : 'Enable';
                _this.campaignStatus = 'active';
            }
            else {
                alert("Error: " + data.code);
            }
        }, function (error) { _this.campaignStatus = 'error'; }, // in case of failure show this message
        function () { } //run this code in all cases
        );
    };
    CampaignComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'campaign',
            templateUrl: 'campaign.component.html',
            providers: [campaign_service_component_1.CampaignService],
            styleUrls: ['campaign.component.css']
        }), 
        __metadata('design:paramtypes', [common_1.Location, http_1.Http, angular2_notifications_1.NotificationsService, (typeof (_a = typeof campaign_service_component_1.CampaignService !== 'undefined' && campaign_service_component_1.CampaignService) === 'function' && _a) || Object])
    ], CampaignComponent);
    return CampaignComponent;
    var _a;
}());
exports.CampaignComponent = CampaignComponent;
//# sourceMappingURL=campaign.component.js.map