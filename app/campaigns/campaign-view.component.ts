import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {CampaignService} from './campaign.service';
import { Http, HttpModule } from '@angular/http';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import * as CryptoJS from 'crypto-js';

@Component({
	moduleId: module.id,
	selector: 'campaign',
	templateUrl: 'campaign-view.component.html',
	providers :[CampaignService],
	styleUrls : ['campaign-view.component.css']
})
export class CampaignComponent implements OnInit {
	constructor(private location:Location, private http:Http, private notif:NotificationsService, private campaignService:CampaignService) {}

    campaign:any;
	campaignStatus:string = 'loading';
    campaignId:number = 0;
    categories:Array<any>;
    proposals:Array<any>;
    proposalsExist:boolean;
    cloudinaryAPISecret:string ='zwZBoDciK30vckcAlhcYV2kodSU';
    enableOrDisable:string;
    enableOrDisableMsg:string;
    updatedCampaign:Object = {};
    endDate:string;
    pageStatus:string = 'loading';
    bidAccepted:boolean = false;
    submitted:boolean = false;

	ngOnInit(){
        this.campaignId = +location.pathname.split('/')[2];
		this.getCategoriesList();
        this.getProposalsList();
        this.getDataFromServer();
	}

    getCategoriesList() {
        this.campaignService.getCategories()
        .subscribe(
            (data) => {

                if(data.code === 200) {
                    this.categories = data.data;
                }
                else {
                    alert(`Error: ${data.code}`);
                }
            },
            error => {},
            () => {}
        );
    }

    getProposalsList() {
        this.campaignService.getProposals(this.campaignId)
        .subscribe(
            (data) => {
                // console.log(data);
                if(data.code === 200) {
                    this.proposalsExist = true;
                    this.proposals = data.data;

                    // check if any bid is accepted
                    for(var i=0; i<this.proposals.length; i++) {
                        // console.log(this.proposals[i].status);
                        if(this.proposals[i].status === 'accepted'){
                            this.bidAccepted = true; 
                        }
                        if(typeof this.proposals[i].channels === 'number') {
                            this.proposals[i].channels = [];
                        }
                        for(var j=0; j<this.proposals[i].channels.length; j++) {
                            console.log(this.proposals[i].channels[j]);
                            if(this.proposals[i].channels[j].type === 'instagramrepost') {
                                this.proposals[i].channels[j].type = 'instagram';  
                            } 
                        }
                    }

                }
                else {
                    this.proposalsExist = false;
                }
            },
            error => {},
            () => {}
        );
    }

    updateAndUploadImageToCloud(fileInput: any, campaign:any){

           if (fileInput.target.files && fileInput.target.files[0]) {
                var reader = new FileReader();
                reader.onload =  (e : any) => {
                    let uploadData:Object = {
                        timestamp: +new Date(),
                        api_key:'116931532498815',
                        file:e.target.result,
                        signature:CryptoJS.SHA1(`timestamp=${+new Date()}${this.cloudinaryAPISecret}`).toString()
                    };

                    this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload',uploadData).subscribe((resp) =>{
                        campaign.campaign_image = resp.json().secure_url;
                    });
                }
                reader.readAsDataURL(fileInput.target.files[0]);
            };
    }

    updateCampaign(campaign:any){
        this.pageStatus = 'loading';
        this.submitted = true;
        campaign.campaign_id = this.campaign.id;
        if(campaign.end_date.length <= 10) {
            campaign.end_date = campaign.end_date + " 23:59:59";
        }

        this.campaignService.updateCampaign(campaign)
        .subscribe((resp) =>{
            if(resp.json().code === 200 ){
                console.log(resp.json());
                this.notif.success('Success!', 'Campaign updated');
                this.pageStatus = 'active';
                setTimeout(() => { (<HTMLInputElement>document.getElementById('end_date')).value = campaign.end_date.slice(0,10); }, 0);
            }
            else {
                this.notif.error('Failure!',resp.json().message);
                this.pageStatus = 'error';
            }
        });
    }

    disableOrEnableCampaign(campaign:any) {
        // this.pageStatus = 'loading';
        this.campaignService.getCampaignDetails(this.campaignId)
        .subscribe(
            (data) => {
                    if(data.code === 200) {
                        this.campaign = data;
                        this.campaign.active = !this.campaign.active;
                        this.campaign.campaign_id = this.campaign.id;
                        // console.log(this.campaign);

                        this.campaignService.updateCampaign(this.campaign)
                        .subscribe((resp) =>{
                            if(resp.json().code === 200 ){
                                // console.log(resp);
                                this.enableOrDisable = this.campaign.active ? 'Disable' : 'Enable';
                                this.enableOrDisableMsg = this.campaign.active ? 'Campaign Enabled' : 'Campaign Disabled';
                                this.notif.success('Success!!', this.enableOrDisableMsg);
                            }
                            else {
                                // console.log(resp);
                                this.notif.error('Failure!!',resp.json().message);
                            }
                        });

                        this.pageStatus = 'active';                      
                    }
                    else {
                        alert(`Error: ${data.code}`);
                    }

                },
            error => { this.campaignStatus = 'error' },
            () => {}
        );
    }

	getDataFromServer() {
        this.campaignService.getCampaignDetails(this.campaignId)
            .subscribe(
                (data) => {

                    if(data.code === 200) {
                        this.campaign = data;
                        // console.log(this.campaign);
                        
                        // Enable/Disable button
                        this.enableOrDisable = this.campaign.active ? 'Disable' : 'Enable';
                        
                        // Loader status
                        this.campaignStatus = 'active';
                        this.pageStatus = 'active';

                        // Set end date in calendar
                        this.endDate = this.campaign.end_date.slice(0,10);
                        setTimeout(() => { (<HTMLInputElement>document.getElementById('end_date')).value = this.endDate; }, 0);
                        
                    }
                    else {
                        alert(`Error: ${data.code}`);
                    }

                },
                error => { this.campaignStatus = 'error' },
                () => {}
            );
    }
}
