"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var campaign_service_1 = require('./campaign.service');
var http_1 = require('@angular/http');
var angular2_notifications_1 = require('angular2-notifications');
var CryptoJS = require('crypto-js');
var CampaignComponent = (function () {
    function CampaignComponent(location, http, notif, campaignService) {
        this.location = location;
        this.http = http;
        this.notif = notif;
        this.campaignService = campaignService;
        this.campaignStatus = 'loading';
        this.campaignId = 0;
        this.cloudinaryAPISecret = 'zwZBoDciK30vckcAlhcYV2kodSU';
        this.updatedCampaign = {};
        this.pageStatus = 'loading';
        this.bidAccepted = false;
        this.submitted = false;
    }
    CampaignComponent.prototype.ngOnInit = function () {
        this.campaignId = +location.pathname.split('/')[2];
        this.getCategoriesList();
        this.getProposalsList();
        this.getDataFromServer();
    };
    CampaignComponent.prototype.getCategoriesList = function () {
        var _this = this;
        this.campaignService.getCategories()
            .subscribe(function (data) {
            if (data.code === 200) {
                _this.categories = data.data;
            }
            else {
                alert("Error: " + data.code);
            }
        }, function (error) { }, function () { });
    };
    CampaignComponent.prototype.getProposalsList = function () {
        var _this = this;
        this.campaignService.getProposals(this.campaignId)
            .subscribe(function (data) {
            // console.log(data);
            if (data.code === 200) {
                _this.proposalsExist = true;
                _this.proposals = data.data;
                // check if any bid is accepted
                for (var i = 0; i < _this.proposals.length; i++) {
                    // console.log(this.proposals[i].status);
                    if (_this.proposals[i].status === 'accepted') {
                        _this.bidAccepted = true;
                    }
                    if (typeof _this.proposals[i].channels === 'number') {
                        _this.proposals[i].channels = [];
                    }
                    for (var j = 0; j < _this.proposals[i].channels.length; j++) {
                        console.log(_this.proposals[i].channels[j]);
                        if (_this.proposals[i].channels[j].type === 'instagramrepost') {
                            _this.proposals[i].channels[j].type = 'instagram';
                        }
                    }
                }
            }
            else {
                _this.proposalsExist = false;
            }
        }, function (error) { }, function () { });
    };
    CampaignComponent.prototype.updateAndUploadImageToCloud = function (fileInput, campaign) {
        var _this = this;
        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var uploadData = {
                    timestamp: +new Date(),
                    api_key: '116931532498815',
                    file: e.target.result,
                    signature: CryptoJS.SHA1("timestamp=" + +new Date() + _this.cloudinaryAPISecret).toString()
                };
                _this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload', uploadData).subscribe(function (resp) {
                    campaign.campaign_image = resp.json().secure_url;
                });
            };
            reader.readAsDataURL(fileInput.target.files[0]);
        }
        ;
    };
    CampaignComponent.prototype.updateCampaign = function (campaign) {
        var _this = this;
        this.pageStatus = 'loading';
        this.submitted = true;
        campaign.campaign_id = this.campaign.id;
        if (campaign.end_date.length <= 10) {
            campaign.end_date = campaign.end_date + " 23:59:59";
        }
        this.campaignService.updateCampaign(campaign)
            .subscribe(function (resp) {
            if (resp.json().code === 200) {
                console.log(resp.json());
                _this.notif.success('Success!', 'Campaign updated');
                _this.pageStatus = 'active';
                setTimeout(function () { document.getElementById('end_date').value = campaign.end_date.slice(0, 10); }, 0);
            }
            else {
                _this.notif.error('Failure!', resp.json().message);
                _this.pageStatus = 'error';
            }
        });
    };
    CampaignComponent.prototype.disableOrEnableCampaign = function (campaign) {
        var _this = this;
        // this.pageStatus = 'loading';
        this.campaignService.getCampaignDetails(this.campaignId)
            .subscribe(function (data) {
            if (data.code === 200) {
                _this.campaign = data;
                _this.campaign.active = !_this.campaign.active;
                _this.campaign.campaign_id = _this.campaign.id;
                // console.log(this.campaign);
                _this.campaignService.updateCampaign(_this.campaign)
                    .subscribe(function (resp) {
                    if (resp.json().code === 200) {
                        // console.log(resp);
                        _this.enableOrDisable = _this.campaign.active ? 'Disable' : 'Enable';
                        _this.enableOrDisableMsg = _this.campaign.active ? 'Campaign Enabled' : 'Campaign Disabled';
                        _this.notif.success('Success!!', _this.enableOrDisableMsg);
                    }
                    else {
                        // console.log(resp);
                        _this.notif.error('Failure!!', resp.json().message);
                    }
                });
                _this.pageStatus = 'active';
            }
            else {
                alert("Error: " + data.code);
            }
        }, function (error) { _this.campaignStatus = 'error'; }, function () { });
    };
    CampaignComponent.prototype.getDataFromServer = function () {
        var _this = this;
        this.campaignService.getCampaignDetails(this.campaignId)
            .subscribe(function (data) {
            if (data.code === 200) {
                _this.campaign = data;
                // console.log(this.campaign);
                // Enable/Disable button
                _this.enableOrDisable = _this.campaign.active ? 'Disable' : 'Enable';
                // Loader status
                _this.campaignStatus = 'active';
                _this.pageStatus = 'active';
                // Set end date in calendar
                _this.endDate = _this.campaign.end_date.slice(0, 10);
                setTimeout(function () { document.getElementById('end_date').value = _this.endDate; }, 0);
            }
            else {
                alert("Error: " + data.code);
            }
        }, function (error) { _this.campaignStatus = 'error'; }, function () { });
    };
    CampaignComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'campaign',
            templateUrl: 'campaign-view.component.html',
            providers: [campaign_service_1.CampaignService],
            styleUrls: ['campaign-view.component.css']
        }), 
        __metadata('design:paramtypes', [common_1.Location, http_1.Http, angular2_notifications_1.NotificationsService, campaign_service_1.CampaignService])
    ], CampaignComponent);
    return CampaignComponent;
}());
exports.CampaignComponent = CampaignComponent;
//# sourceMappingURL=campaign-view.component.js.map