"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var secure_http_service_1 = require('../secure-http.service');
var core_1 = require('@angular/core');
var CampaignService = (function () {
    function CampaignService(authHttp) {
        this.authHttp = authHttp;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
    }
    CampaignService.prototype.getCampaignsList = function (sort, page, value) {
        return this.authHttp.get(this.baseURL + "/admin/listcampaigns?total=10&page=" + page + "&sort=" + sort + "&search_key=" + value)
            .map(function (resp) { return resp.json(); });
    };
    CampaignService.prototype.getCampaignDetails = function (id) {
        return this.authHttp.get(this.baseURL + "/campaigndetails?campaign_id=" + id)
            .map(function (resp) { return resp.json(); });
    };
    CampaignService.prototype.getCategories = function () {
        return this.authHttp.get(this.baseURL + "/listcategories")
            .map(function (resp) { return resp.json(); });
    };
    CampaignService.prototype.getProposals = function (id) {
        return this.authHttp.get(this.baseURL + "/getproposals?campaign_id=" + id + "&sort=dsccrt&total=10&page=1&influencer_id=")
            .map(function (resp) { return resp.json(); });
    };
    CampaignService.prototype.updateCampaign = function (campaign) {
        return this.authHttp.post(this.baseURL + "/updatecampaign", campaign);
    };
    CampaignService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [secure_http_service_1.AuthHttp])
    ], CampaignService);
    return CampaignService;
}());
exports.CampaignService = CampaignService;
//# sourceMappingURL=campaign.service.js.map