"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var campaign_service_1 = require('./campaign.service');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
require('rxjs/add/operator/switchMap');
var CampaignsComponent = (function () {
    function CampaignsComponent(campaignService, location, router, activatedRoute) {
        var _this = this;
        this.campaignService = campaignService;
        this.location = location;
        this.router = router;
        this.activatedRoute = activatedRoute;
        // Declaration of Campaigns List variables
        // Campaigns List data loading status
        this.campaignsStatus = 'loading';
        this.pageStatus = 'loading';
        // Latest Campaigns
        this.campaignsList = [];
        this.campaignsListModified = [];
        this.channelName = '';
        // Pagination variables
        this.campaignsPerPage = 10;
        this.pageNumber = 1;
        this.pageNumberItem = null;
        this.searchKey = '';
        // Date conversion: Format e.g. 03 Oct '16'
        this.convertDate = function (date) {
            var day = '', month = '', year = '', endDate = '';
            var dateObject = new Date();
            var modDate = '';
            dateObject = new Date(date);
            modDate = dateObject.toString();
            day = modDate.slice(8, 10);
            month = modDate.slice(4, 7);
            year = modDate.slice(13, 15);
            endDate = day + " " + month + " '" + year;
            return endDate;
        };
        // Get campaigns list api call
        this.getCampaignsList = function () {
            _this.campaignService.getCampaignsList(_this.sortfield, _this.pageNumber, _this.searchKey)
                .subscribe(function (data) {
                if (data.code === 611) {
                    _this.pageStatus = 'notFound';
                }
                else if (data.code === 200) {
                    // Data fetched from the api
                    _this.campaignsData = data;
                    // console.log(this.campaignsData);
                    // Total No. of campaigns
                    _this.totalCampaigns = _this.campaignsData.pagination.count;
                    // Set total pages and create page numbers for pagination
                    _this.totalPages = Math.ceil(_this.totalCampaigns / _this.campaignsPerPage);
                    _this.pageNumbers = Array.from(new Array(_this.totalPages), function (val, index) { return index + 1; });
                    // List of campaigns
                    _this.campaignsList = _this.campaignsData.data;
                    _this.campaignsListModified = _this.campaignsList;
                    // Modified Latest Campaigns
                    for (var i = 0; i < _this.campaignsList.length; i++) {
                        _this.campaignsListModified[i].end_date = _this.convertDate(_this.campaignsListModified[i].end_date);
                        if (_this.campaignsListModified[i].brand_name === null) {
                            _this.campaignsListModified[i].brand_name = 'N/A';
                        }
                        _this.campaignsListModified[i].budget = 0;
                        // If social channels array is null, set length to 0 for loop iteration
                        (_this.campaignsListModified[i].channels === null) ? _this.channelsLength = 0 : _this.channelsLength = _this.campaignsListModified[i].channels.length;
                        // Social Media Channels Display Logic
                        for (var j = 0; j < _this.channelsLength; j++) {
                            _this.campaignsListModified[i].budget += +_this.campaignsListModified[i].channels[j].max_amount;
                            _this.channelName = _this.campaignsListModified[i].channels[j].type;
                        }
                    }
                    _this.campaignsStatus = 'active';
                    _this.pageStatus = 'active';
                    // Page number highlight logic
                    _this.pageNumberItem = "page" + _this.pageNumber;
                    if (_this.pageNumberItem !== null) {
                        setTimeout(function () { document.getElementById(_this.pageNumberItem).className = 'active'; }, 0);
                    }
                }
                else {
                    console.log(data);
                    alert("Error: " + data.code);
                }
            }, function (error) { _this.campaignsStatus = 'error'; }, // in case of failure show this message
            function () { } //run this code in all cases
            );
        };
        this.location = location;
    }
    CampaignsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.activatedRoute
            .queryParams
            .subscribe(function (params) {
            _this.sortfield = params['sort'] || 'ascid';
            _this.pageNumber = +params['page'] || 1;
            _this.searchKey = params['key'] || '';
        });
        // Initial api call to get first page of campaigns list: pageNumber = 1
        this.firstPage(this.pageNumber);
    };
    CampaignsComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    CampaignsComponent.prototype.sort = function (sorts) {
        this.pageNumber = 1;
        // Toggle sort order logic
        if (this.sortfield === 'ascid' && sorts === 'ascid')
            sorts = 'dscid';
        else if (this.sortfield === 'dscid' && sorts === 'dscid')
            sorts = 'ascid';
        if (this.sortfield === 'ascend' && sorts === 'ascend')
            sorts = 'dscend';
        else if (this.sortfield === 'dscend' && sorts === 'dscend')
            sorts = 'ascend';
        else if (this.sortfield === 'ascname' && sorts === 'ascname')
            sorts = 'dscname';
        else if (this.sortfield === 'dscname' && sorts === 'dscname')
            sorts = 'ascname';
        else if (this.sortfield === 'ascbname' && sorts === 'ascbname')
            sorts = 'dscbname';
        else if (this.sortfield === 'dscbname' && sorts === 'dscbname')
            sorts = 'ascbname';
        this.sortfield = sorts;
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getCampaignsList();
    };
    // Previous button click handler for pagination
    CampaignsComponent.prototype.previousPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getCampaignsList();
        this.pageNumber--;
    };
    // Next button click handler for pagination
    CampaignsComponent.prototype.nextPage = function (page) {
        console.log(page);
        this.pageNumber = page;
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getCampaignsList();
        this.pageNumber++;
        console.log(this.pageNumber);
    };
    // Load first page initially
    CampaignsComponent.prototype.firstPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getCampaignsList();
    };
    // Page number li element click handler for pagination
    CampaignsComponent.prototype.specificPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getCampaignsList();
    };
    CampaignsComponent.prototype.onSearch = function (value) {
        this.pageNumber = 1;
        this.sortfield = 'ascid';
        this.searchKey = value;
        this.pageStatus = 'loading';
        this.router.navigate(['/campaigns'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.getCampaignsList();
    };
    CampaignsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'campaigns',
            templateUrl: 'campaigns.component.html',
            providers: [campaign_service_1.CampaignService],
            styleUrls: ['campaigns.component.css']
        }), 
        __metadata('design:paramtypes', [campaign_service_1.CampaignService, common_1.Location, router_1.Router, router_1.ActivatedRoute])
    ], CampaignsComponent);
    return CampaignsComponent;
}());
exports.CampaignsComponent = CampaignsComponent;
//# sourceMappingURL=campaigns.component.js.map