import { Component, OnInit } from '@angular/core';
import { CampaignService } from './campaign.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
	moduleId: module.id,
	selector: 'campaigns',
	templateUrl: 'campaigns.component.html',
	providers : [CampaignService],
	styleUrls : ['campaigns.component.css']
})
export class CampaignsComponent implements OnInit {

	// Declaration of Campaigns List variables

	// Campaigns List data loading status
	campaignsStatus:string = 'loading';
	pageStatus:string = 'loading';

	// Campaigns List data returned from the API call
	campaignsData:any;

	// Latest Campaigns
	campaignsList:Array<any> = [];
	campaignsListModified:Array<any> = [];
	channelsLength:number;
	channelName:string = '';

	// Pagination variables
	campaignsPerPage: number = 10;
	pageNumber:number = 1;
	totalCampaigns:number;
	totalPages:number;
	pageNumbers:Array<number>;
	pageNumberItem:any = null;

	// Search and sort variables
	sortfield:any;
	searchKey:string= '';

	// route subscriber
	sub:any;

	constructor(private campaignService:CampaignService, private location:Location , private router:Router, private activatedRoute: ActivatedRoute) {
		this.location = location;
	}

	ngOnInit() {

        this.sub = this.activatedRoute
            .queryParams
            .subscribe(params => {
				this.sortfield = params['sort'] || 'ascid';
                this.pageNumber = +params['page'] || 1;
				this.searchKey = params['key'] || '';
            });

        // Initial api call to get first page of campaigns list: pageNumber = 1
        this.firstPage(this.pageNumber);
    }

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	// Date conversion: Format e.g. 03 Oct '16'
	convertDate = (date:any) => {
		var day = '',
			month = '',
			year = '',
			endDate = '';

		var dateObject = new Date();
		var modDate = '';

		dateObject = new Date(date);

		modDate = dateObject.toString();

		day = modDate.slice(8,10);
		month = modDate.slice(4,7);
		year = modDate.slice(13,15);

		endDate = `${day} ${month} '${year}`;

		return endDate;
	}

	sort(sorts:any){
		this.pageNumber = 1;

		// Toggle sort order logic
		if(this.sortfield==='ascid' && sorts ==='ascid')
			sorts='dscid'
		else if(this.sortfield==='dscid'  && sorts ==='dscid')
			sorts='ascid'
		if(this.sortfield==='ascend' && sorts ==='ascend')
			sorts='dscend'
		else if(this.sortfield==='dscend'  && sorts ==='dscend')
			sorts='ascend'
		else if(this.sortfield==='ascname' && sorts ==='ascname')
			sorts='dscname'
		else if(this.sortfield==='dscname' && sorts ==='dscname')
			sorts='ascname'
		else if(this.sortfield==='ascbname' && sorts ==='ascbname')
			sorts='dscbname'
		else if(this.sortfield==='dscbname' && sorts ==='dscbname')
			sorts='ascbname'

		this.sortfield = sorts;
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getCampaignsList();
	}


	// Previous button click handler for pagination
	previousPage(page:number) {

		this.pageNumber = page;
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getCampaignsList();
		this.pageNumber--;
	}

	// Next button click handler for pagination
	nextPage(page:number) {
		console.log(page);
		this.pageNumber = page;
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getCampaignsList();
		this.pageNumber++;
		console.log(this.pageNumber);
	}

	// Load first page initially
	firstPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getCampaignsList();
	}

	// Page number li element click handler for pagination
	specificPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getCampaignsList();
	}

	onSearch(value: string){
		this.pageNumber = 1;
		this.sortfield = 'ascid';
		this.searchKey = value;
		this.pageStatus = 'loading';
		this.router.navigate(['/campaigns'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
        this.getCampaignsList();
    }

	// Get campaigns list api call
	getCampaignsList = () => {

        this.campaignService.getCampaignsList(this.sortfield, this.pageNumber, this.searchKey)
            .subscribe(
                (data:any) => {

                    if(data.code === 611) {
                        this.pageStatus = 'notFound';
                    }

                	// Succesfully fetched data
                	else if(data.code === 200) {

                		// Data fetched from the api
                		this.campaignsData = data;
                		// console.log(this.campaignsData);

                		// Total No. of campaigns
	                	this.totalCampaigns = this.campaignsData.pagination.count;

	                	// Set total pages and create page numbers for pagination
	                	this.totalPages = Math.ceil(this.totalCampaigns / this.campaignsPerPage);
						this.pageNumbers = Array.from(new Array(this.totalPages),(val,index)=>index+1);

	                	// List of campaigns
	                	this.campaignsList = this.campaignsData.data;
	                	this.campaignsListModified = this.campaignsList;

	                	// Modified Latest Campaigns
	                	for(var i=0; i<this.campaignsList.length; i++) {


	                		this.campaignsListModified[i].end_date = this.convertDate(this.campaignsListModified[i].end_date);

	                		if(this.campaignsListModified[i].brand_name === null) {
                				this.campaignsListModified[i].brand_name = 'N/A';
                			}

	                		this.campaignsListModified[i].budget = 0;

	                		// If social channels array is null, set length to 0 for loop iteration
	                		( this.campaignsListModified[i].channels === null ) ? this.channelsLength = 0 : this.channelsLength = this.campaignsListModified[i].channels.length;

	                		// Social Media Channels Display Logic
	                		for(var j=0; j<this.channelsLength; j++) {
	                			this.campaignsListModified[i].budget += +this.campaignsListModified[i].channels[j].max_amount;
	                			this.channelName = this.campaignsListModified[i].channels[j].type;
	                		}
	                	}

	                	this.campaignsStatus = 'active';
	                	this.pageStatus = 'active';

	                	// Page number highlight logic
	                	this.pageNumberItem = "page" + this.pageNumber;
						if(this.pageNumberItem !== null) {
							setTimeout(() => { document.getElementById(this.pageNumberItem).className = 'active'; }, 0);
						}
                	}

                	// Error fetching data
                	else {
                		console.log(data);
                		alert(`Error: ${data.code}`);
                	}

                },
                error => { this.campaignsStatus = 'error' }, // in case of failure show this message
                () => {}//run this code in all cases
            );

    }


}
