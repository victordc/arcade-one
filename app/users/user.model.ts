// blue print of user
export class UserModel {
      email: string; // bind from ng model in form
      password: string; // same
      first_name: string; // same
      last_name: string; // same
      role: string = 'admin'; // constant value so assign it here
      type: string = 'email'; // constant value assign it here
      confirm_password?:string;
}
