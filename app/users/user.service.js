"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var secure_http_service_1 = require('../secure-http.service');
var UserService = (function () {
    function UserService(auth) {
        this.auth = auth;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
    }
    UserService.prototype.fetchUsers = function (page, sort, value) {
        return this.auth.get(this.baseURL + "/admin/listusers?total=10&page=" + page + "&sort=" + sort + "&role=admin&search_key=" + value);
    };
    UserService.prototype.fetchUser = function (id) {
        return this.auth.get(this.baseURL + "/userdetails?user_id=" + id);
    };
    UserService.prototype.addUser = function (user) {
        return this.auth.post(this.baseURL + "/register", user);
    };
    UserService.prototype.updateUser = function (user) {
        return this.auth.post(this.baseURL + "/updateuser", user);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [secure_http_service_1.AuthHttp])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map