//Important points to note while programming form component
//the following component controls the user form whole
//we need to display, listen and extract at the same time
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var user_service_1 = require('../users/user.service');
var user_model_1 = require('../users/user.model');
var router_1 = require('@angular/router');
var angular2_notifications_1 = require('angular2-notifications');
var UserFormComponent = (function () {
    function UserFormComponent(userServiceAdd, router, notif) {
        this.userServiceAdd = userServiceAdd;
        this.router = router;
        this.notif = notif;
        this.user = new user_model_1.UserModel();
        this.emailParadigm = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        this.submitted = false;
    }
    UserFormComponent.prototype.onSubmit = function (user) {
        var _this = this;
        this.submitted = true;
        this.userServiceAdd.addUser(user).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.notif.success('Success!!', 'User added');
                _this.router.navigateByUrl('/users');
            }
            else {
                _this.notif.error('Failure!!', 'Unable to add');
            }
        });
    };
    UserFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user-form',
            templateUrl: 'user-form.component.html',
            styleUrls: ['user-form.component.css'],
            providers: [http_1.Http, user_service_1.UserService, user_model_1.UserModel]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router, angular2_notifications_1.NotificationsService])
    ], UserFormComponent);
    return UserFormComponent;
}());
exports.UserFormComponent = UserFormComponent;
//Important Notes
//WHY SEPARATE TEMPLATE FILE ?
/*
    Typescript and Javascript files generally aren't the best place to write (or read) large strctches of HTML
*/
//# sourceMappingURL=user-form.component.js.map