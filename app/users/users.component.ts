import { Component, OnInit,trigger, transition, style, animate, state } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from '../users/user.service';

@Component({
	moduleId: module.id,
	selector: 'users',
	animations: [
	    trigger(
	      'myAnimation', [
	        transition(':enter', [
	          style({opacity: 0}),
	          animate('100ms', style({opacity: 1}))
	        ]),
	        transition(':leave', [
	          style({opacity: 1}),
	          animate('100ms', style({opacity: 0}))
	        ])
	      ]
	    )
	],
	templateUrl: 'users.component.html',
	styleUrls:['users.component.css'],
	providers:[Http,UserService]

})

export class UsersComponent implements OnInit {

    users:Array<any>;
	sortfield:string = 'ascid';
    entities:boolean = false;
    userStatus:string='loading';
    pageStatus:string='loading';
	searchKey:string = '';

	// Pagination variables
	usersPerPage: number = 10;
	pageNumber:number = 1;
	totalUsers:number;
	totalPages:number;
	pageNumbers:Array<number>;
	pageNumberItem:any = null;

	constructor(private user: UserService, private activatedRoute: ActivatedRoute, private router:Router) { }

	ngOnInit() {

		this.activatedRoute.queryParams.subscribe((params: Params) => {
			this.sortfield = params['sort'] || this.sortfield;
			this.pageNumber = +params['page'] || 1;
            this.searchKey = params['key'] || this.searchKey;
		});
		//get users here
		this.specificPage(this.pageNumber);
	}

	sort(sorts:any){
		this.pageNumber = 1;

		// Toggle sort order logic
		if(this.sortfield==='ascid' && sorts ==='ascid')
			sorts='dscid'
		else if(this.sortfield==='dscid'  && sorts ==='dscid')
			sorts='ascid'
		if(this.sortfield==='asccrt' && sorts ==='asccrt')
			sorts='dsccrt'
		else if(this.sortfield==='dsccrt'  && sorts ==='dsccrt')
			sorts='asccrt'
		else if(this.sortfield==='ascname' && sorts ==='ascname')
			sorts='dscname'
		else if(this.sortfield==='dscname' && sorts ==='dscname')
			sorts='ascname'
		else if(this.sortfield==='ascemail' && sorts ==='ascemail')
			sorts='dscemail'
		else if(this.sortfield==='dscemail' && sorts ==='dscemail')
			sorts='ascemail'

		this.sortfield = sorts;
		this.router.navigate(['/users'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getUsersList();
	}

	getUsersList(){
		this.user.fetchUsers(this.pageNumber, this.sortfield, this.searchKey).subscribe(
            r => {
                if(r.json().code === 611)
                    this.pageStatus = 'notFound';
            	// console.log(r.json());
				// Total No. of brands
				this.totalUsers = r.json().pagination.count;
				// Set total pages and create page numbers for pagination
            	this.totalPages = Math.ceil(this.totalUsers / this.usersPerPage);
				this.pageNumbers = Array.from(new Array(this.totalPages),(val,index)=>index+1);

				this.users = r.json().data;
				this.userStatus='active';
                this.pageStatus='active';
				this.entities = true;

				// Page number highlight logic
            	this.pageNumberItem = "page" + this.pageNumber;
				if(this.pageNumberItem !== null) {
					setTimeout(() => { document.getElementById(this.pageNumberItem).className = 'active'; }, 0);
				}
			},
            error => {
                this.userStatus = 'error';
                this.pageStatus = 'error';
            }
		);
	}

    onSearch(value: string){
		this.pageNumber = 1;
		this.sortfield = 'ascid';
		this.searchKey = value;
		this.pageStatus = 'loading';
		this.router.navigate(['/users'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.getUsersList();
    }

    // Previous button click handler for pagination
	previousPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/users'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getUsersList();
		// this.pageNumber--;
	}

	// Next button click handler for pagination
	nextPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/users'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getUsersList();
		// this.pageNumber++;
	}

	// Page number li element click handler for pagination
	specificPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/users'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getUsersList();
	}


}
