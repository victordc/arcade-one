import { Injectable } from '@angular/core';
import { AuthHttp } from '../secure-http.service';

@Injectable()

export class UserService {

	baseURL='http://instatise-server-dev.herokuapp.com/api';

	constructor(private auth:AuthHttp) { }
	fetchUsers(page:number, sort:string, value:string){
		return this.auth.get(`${this.baseURL}/admin/listusers?total=10&page=${page}&sort=${sort}&role=admin&search_key=${value}`);

	}
	fetchUser(id:number){
		return this.auth.get(`${this.baseURL}/userdetails?user_id=${id}`);
	}
	addUser(user:Object){
		return this.auth.post(`${this.baseURL}/register`,user);
	}
	updateUser(user:Object){
		return this.auth.post(`${this.baseURL}/updateuser`,user);
	}
}
