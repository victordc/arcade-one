"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('../users/user.service');
var user_model_1 = require('../users/user.model');
var angular2_notifications_1 = require('angular2-notifications');
var common_1 = require('@angular/common');
var UserComponent = (function () {
    function UserComponent(userService, notif, location) {
        this.userService = userService;
        this.notif = notif;
        this.location = location;
        this.user = new user_model_1.UserModel();
        this.entities = false;
        this.loader = 'loading';
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        var idOfUser = +location.pathname.split('/')[2];
        //get user details
        this.userService.fetchUser(idOfUser).subscribe(function (resp) {
            _this.user = resp.json();
            _this.enableOrDisable = resp.json().active === false ? 'enable' : 'disable';
            _this.loader = 'active';
            _this.entities = true;
        }, function (error) { _this.loader = 'error'; });
    };
    ;
    UserComponent.prototype.updateUser = function (user) {
        var _this = this;
        this.userService.updateUser(user).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.notif.success('Success!!', resp.json().message); //sucess //heading //description
            }
            else {
                _this.notif.error('Failure!!', resp.json().message);
            }
        });
    };
    ;
    UserComponent.prototype.disableOrEnableUser = function (user) {
        var _this = this;
        user.active = !user.active;
        this.userService.updateUser(user).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.userService.fetchUser(resp.json().user_id).subscribe(function (resp) {
                    _this.user = resp.json();
                    _this.enableOrDisable = resp.json().active === false ? 'enable' : 'disable';
                    _this.enableOrDisableMsg = resp.json().active === false ? ' User disabled' : 'User enabled';
                    _this.notif.success('Success!!', _this.enableOrDisableMsg); //sucess //heading //description
                });
            }
            else {
                _this.notif.error('Failure!!', "");
            }
        });
    };
    ;
    UserComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user',
            animations: [
                core_1.trigger('myAnimation', [
                    core_1.transition(':enter', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate('100ms', core_1.style({ opacity: 1 }))
                    ]),
                    core_1.transition(':leave', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate('100ms', core_1.style({ opacity: 0 }))
                    ])
                ])
            ],
            templateUrl: 'user.component.html',
            providers: [user_service_1.UserService, user_model_1.UserModel],
            styleUrls: ['user.component.css']
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, angular2_notifications_1.NotificationsService, common_1.Location])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map