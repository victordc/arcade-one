//Important points to note while programming form component
//the following component controls the user form whole
//we need to display, listen and extract at the same time

import { Component, OnInit,trigger, transition, style, animate, state, Injectable, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../users/user.service';
import { UserModel} from '../users/user.model';
import { Router } from '@angular/router';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';

@Component({
	moduleId: module.id,
	selector: 'user-form',
	templateUrl: 'user-form.component.html',
	styleUrls:['user-form.component.css'],
	providers:[Http,UserService,UserModel]
})

export class UserFormComponent {

		user = new UserModel();
    emailParadigm = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    submitted = false;
		emailValidation:boolean;

		constructor(private userServiceAdd: UserService,
							  private router:Router,
              	private notif:NotificationsService) {}

    onSubmit(user:any){
        this.submitted = true;
        this.userServiceAdd.addUser(user).subscribe((resp) => {
            if(resp.json().code === 200){
                this.notif.success('Success!!','User added');
                this.router.navigateByUrl('/users');
            }else{
                this.notif.error('Failure!!','Unable to add')
            }
        });
    }

}

//Important Notes
//WHY SEPARATE TEMPLATE FILE ?
/*
    Typescript and Javascript files generally aren't the best place to write (or read) large strctches of HTML
*/
