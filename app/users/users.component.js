"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var user_service_1 = require('../users/user.service');
var UsersComponent = (function () {
    function UsersComponent(user, activatedRoute, router) {
        this.user = user;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sortfield = 'ascid';
        this.entities = false;
        this.userStatus = 'loading';
        this.pageStatus = 'loading';
        this.searchKey = '';
        // Pagination variables
        this.usersPerPage = 10;
        this.pageNumber = 1;
        this.pageNumberItem = null;
    }
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.sortfield = params['sort'] || _this.sortfield;
            _this.pageNumber = +params['page'] || 1;
            _this.searchKey = params['key'] || _this.searchKey;
        });
        //get users here
        this.specificPage(this.pageNumber);
    };
    UsersComponent.prototype.sort = function (sorts) {
        this.pageNumber = 1;
        // Toggle sort order logic
        if (this.sortfield === 'ascid' && sorts === 'ascid')
            sorts = 'dscid';
        else if (this.sortfield === 'dscid' && sorts === 'dscid')
            sorts = 'ascid';
        if (this.sortfield === 'asccrt' && sorts === 'asccrt')
            sorts = 'dsccrt';
        else if (this.sortfield === 'dsccrt' && sorts === 'dsccrt')
            sorts = 'asccrt';
        else if (this.sortfield === 'ascname' && sorts === 'ascname')
            sorts = 'dscname';
        else if (this.sortfield === 'dscname' && sorts === 'dscname')
            sorts = 'ascname';
        else if (this.sortfield === 'ascemail' && sorts === 'ascemail')
            sorts = 'dscemail';
        else if (this.sortfield === 'dscemail' && sorts === 'dscemail')
            sorts = 'ascemail';
        this.sortfield = sorts;
        this.router.navigate(['/users'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getUsersList();
    };
    UsersComponent.prototype.getUsersList = function () {
        var _this = this;
        this.user.fetchUsers(this.pageNumber, this.sortfield, this.searchKey).subscribe(function (r) {
            if (r.json().code === 611)
                _this.pageStatus = 'notFound';
            // console.log(r.json());
            // Total No. of brands
            _this.totalUsers = r.json().pagination.count;
            // Set total pages and create page numbers for pagination
            _this.totalPages = Math.ceil(_this.totalUsers / _this.usersPerPage);
            _this.pageNumbers = Array.from(new Array(_this.totalPages), function (val, index) { return index + 1; });
            _this.users = r.json().data;
            _this.userStatus = 'active';
            _this.pageStatus = 'active';
            _this.entities = true;
            // Page number highlight logic
            _this.pageNumberItem = "page" + _this.pageNumber;
            if (_this.pageNumberItem !== null) {
                setTimeout(function () { document.getElementById(_this.pageNumberItem).className = 'active'; }, 0);
            }
        }, function (error) {
            _this.userStatus = 'error';
            _this.pageStatus = 'error';
        });
    };
    UsersComponent.prototype.onSearch = function (value) {
        this.pageNumber = 1;
        this.sortfield = 'ascid';
        this.searchKey = value;
        this.pageStatus = 'loading';
        this.router.navigate(['/users'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.getUsersList();
    };
    // Previous button click handler for pagination
    UsersComponent.prototype.previousPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/users'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getUsersList();
        // this.pageNumber--;
    };
    // Next button click handler for pagination
    UsersComponent.prototype.nextPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/users'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getUsersList();
        // this.pageNumber++;
    };
    // Page number li element click handler for pagination
    UsersComponent.prototype.specificPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/users'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getUsersList();
    };
    UsersComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'users',
            animations: [
                core_1.trigger('myAnimation', [
                    core_1.transition(':enter', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate('100ms', core_1.style({ opacity: 1 }))
                    ]),
                    core_1.transition(':leave', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate('100ms', core_1.style({ opacity: 0 }))
                    ])
                ])
            ],
            templateUrl: 'users.component.html',
            styleUrls: ['users.component.css'],
            providers: [http_1.Http, user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.ActivatedRoute, router_1.Router])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.component.js.map