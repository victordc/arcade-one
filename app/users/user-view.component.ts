import { Component, OnInit,trigger, transition, style, animate, state } from '@angular/core';
import { UserService } from '../users/user.service';
import { UserModel } from '../users/user.model';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Location } from '@angular/common';

@Component({
	moduleId: module.id,
	selector: 'user',
	animations: [
	    trigger(
	      'myAnimation', [
	        transition(':enter', [
	          style({opacity: 0}),
	          animate('100ms', style({opacity: 1}))
	        ]),
	        transition(':leave', [
	          style({opacity: 1}),
	          animate('100ms', style({opacity: 0}))
	        ])
	      ]
	    )
	],
	templateUrl: 'user-view.component.html',
	providers:[UserService,UserModel],
	styleUrls:['user-view.component.css']
})

export class UserComponent implements OnInit {

	user:UserModel = new UserModel();
  emailParadigm = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
	currentState:string;
	entities:boolean = false;
	enableOrDisable:string;
	enableOrDisableMsg:string;
	loader:string='loading';
  pageStatus:string='loading';
  submitted = false;
	passwordDisabled:boolean = true;

	constructor(private userService: UserService,
							private notif:NotificationsService,
              private location:Location) {}

	ngOnInit() {

    let idOfUser = +location.pathname.split('/')[2];
		//get user details
		this.userService.fetchUser(idOfUser).subscribe(
            r => {
							this.passwordDisabled = +localStorage.getItem('user_id') === r.json().user_id ? false : true;
		    			this.user = r.json();
		    			this.enableOrDisable = r.json().active === false ? 'enable' : 'disable';
		    			this.loader='active';
		          this.pageStatus = 'active';
		    			this.entities = true;
		    		},
            error => {
              this.loader = 'error';
              this.pageStatus = 'error';
            }
        );
	};

	public updateUser(user:Object){
        this.pageStatus = 'loading';
        this.submitted = true;
		this.userService.updateUser(user).subscribe((resp) => {
			if(resp.json().code === 200 ){
				this.notif.success('Success!!',resp.json().message);
                this.pageStatus = 'active';
			}else{
				this.notif.error('Failure!!',resp.json().message);
                this.pageStatus = 'error';
			}
		})
	};

	public disableOrEnableUser(user:any){
		user.active = !user.active;
		this.userService.updateUser(user).subscribe((resp) => {
			if(resp.json().code === 200 ){
				this.userService.fetchUser(resp.json().user_id).subscribe((resp) =>{
					this.user = resp.json();
					this.enableOrDisable = resp.json().active === false ? 'enable' : 'disable';
					this.enableOrDisableMsg = resp.json().active === false ? ' User disabled' : 'User enabled';
					this.notif.success('Success!!',this.enableOrDisableMsg);
				});
			}else{
				this.notif.error('Failure!!',"");
			}
		})
	};

}
