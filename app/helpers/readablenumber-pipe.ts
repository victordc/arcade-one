import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/
@Pipe({name: 'exponentialStrength'})
export class ExponentialStrengthPipe implements PipeTransform {
  transform(value: number, decimals: number): string {
       let exp1:number;
       let rounded:number;
       let suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
      if(!value) {
        return null;
      }

      if(value < 1000) {
        return String(value);
      }
      let digits = String(value).length;
      if(digits>3 && digits < 7)
      exp1 = 1;
      else if ( digits >= 7)
      exp1 = 2;
      return (value / Math.pow(1000, exp1)).toFixed(decimals) + suffixes[exp1-1];
  }
}