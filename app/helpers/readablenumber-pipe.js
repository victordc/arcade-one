"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/
var ExponentialStrengthPipe = (function () {
    function ExponentialStrengthPipe() {
    }
    ExponentialStrengthPipe.prototype.transform = function (value, decimals) {
        var exp1;
        var rounded;
        var suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
        if (!value) {
            return null;
        }
        if (value < 1000) {
            return String(value);
        }
        var digits = String(value).length;
        if (digits > 3 && digits < 7)
            exp1 = 1;
        else if (digits >= 7)
            exp1 = 2;
        return (value / Math.pow(1000, exp1)).toFixed(decimals) + suffixes[exp1 - 1];
    };
    ExponentialStrengthPipe = __decorate([
        core_1.Pipe({ name: 'exponentialStrength' }), 
        __metadata('design:paramtypes', [])
    ], ExponentialStrengthPipe);
    return ExponentialStrengthPipe;
}());
exports.ExponentialStrengthPipe = ExponentialStrengthPipe;
//# sourceMappingURL=readablenumber-pipe.js.map