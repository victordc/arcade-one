import { Component, OnInit,trigger, transition, style, animate, state, ElementRef, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import {InfluencerService} from './influencers.service';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import {ActivatedRoute} from '@angular/router';      
import { Location } from '@angular/common';
import { Http, HttpModule } from '@angular/http';
import * as CryptoJS from 'crypto-js';
@Component({
	moduleId: module.id,
	selector: 'influencer',
	templateUrl: 'influencer-view.component.html',
	providers :[InfluencerService],
	styleUrls : ['influencer-view.component.css']
})
export class InfluencerComponent implements OnInit {
	constructor(private route:ActivatedRoute, private iservice:InfluencerService,private notif:NotificationsService,
                private location:Location, private http:Http) { }
	influencer:any = {};
	currentState:string;
	campLength:number;
	campOpen:number;
	entities:boolean = false;
	cloudinaryAPISecret:string ='zwZBoDciK30vckcAlhcYV2kodSU';
	enableOrDisable:string;
	enableOrDisableMsg:string;
	loader:string='loading';

	ngOnInit() {
		let id:number= 0;
		this.route.params.subscribe(params => {
		console.log('Param whatever: ', params['id']);
		id = params['id'];
		});
		this.iservice.getSingle(id)
			.subscribe((resp)=>{
				this.influencer =resp;
				this.loader='active';
				this.enableOrDisable = resp.active === false ? 'Enable' : 'Disable';
				this.enableOrDisableMsg = resp.active === false ? ' User disabled' : 'User enabled';
				this.entities = true;
				if(resp.campaigns)
				{	
					this.campLength=resp.campaigns.length;
					this.campOpen=resp.campaigns.length;
				}
				console.log(this.influencer);
			},error =>{ this.loader = 'error';})
	 };
	 public updateAndUploadImageToCloud(fileInput: any, brand:any){

           if (fileInput.target.files && fileInput.target.files[0]) {
                var reader = new FileReader();
                reader.onload =  (e : any) => {
                    let uploadData:Object = {
                        timestamp: +new Date(),
                        api_key:'116931532498815',
                        file:e.target.result,
                        signature:CryptoJS.SHA1(`timestamp=${+new Date()}${this.cloudinaryAPISecret}`).toString()
                    };

                    this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload',uploadData).subscribe((resp) =>{
                        brand.profile_picture_url = resp.json().secure_url;
                    });
                }
                reader.readAsDataURL(fileInput.target.files[0]);
            };

    };
	 public updateUser(){
		let user = <any> { user_id:this.influencer.user_id, first_name: this.influencer.first_name, last_name: this.influencer.last_name, bio:this.influencer.bio };
		this.iservice.updateInf(user).subscribe((resp) => {
			if(resp.json().code === 200 ){
				this.notif.success('Success!!',resp.json().message);//sucess //heading //description
			}else{
				this.notif.error('Failure!!',resp.json().message);
			}
		})
	};
	public disableOrEnableInfluencer(){
		this.influencer.active = !this.influencer.active;
		let user = <any> { user_id:this.influencer.user_id, active: this.influencer.active};
		this.iservice.updateInf(this.influencer).subscribe((resp) => {
			if(resp.json().code === 200 ){
				this.iservice.getSingle(resp.json().user_id).subscribe((resp) =>{
					this.influencer = resp;
					this.enableOrDisable = resp.active === false ? 'Enable' : 'Disable';
					this.enableOrDisableMsg = resp.active === false ? ' User Disabled' : 'User Enable';
					this.notif.success('Success!!',this.enableOrDisableMsg);//sucess //heading //description
				});
			}else{
				this.notif.error('Failure!!',"");
			}
		})
	};

	
}