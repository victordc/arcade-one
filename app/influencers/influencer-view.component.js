"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var influencers_service_1 = require('./influencers.service');
var angular2_notifications_1 = require('angular2-notifications');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var http_1 = require('@angular/http');
var CryptoJS = require('crypto-js');
var InfluencerComponent = (function () {
    function InfluencerComponent(route, iservice, notif, location, http) {
        this.route = route;
        this.iservice = iservice;
        this.notif = notif;
        this.location = location;
        this.http = http;
        this.influencer = {};
        this.entities = false;
        this.cloudinaryAPISecret = 'zwZBoDciK30vckcAlhcYV2kodSU';
        this.loader = 'loading';
    }
    InfluencerComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = 0;
        this.route.params.subscribe(function (params) {
            console.log('Param whatever: ', params['id']);
            id = params['id'];
        });
        this.iservice.getSingle(id)
            .subscribe(function (resp) {
            _this.influencer = resp;
            _this.loader = 'active';
            _this.enableOrDisable = resp.active === false ? 'Enable' : 'Disable';
            _this.enableOrDisableMsg = resp.active === false ? ' User disabled' : 'User enabled';
            _this.entities = true;
            if (resp.campaigns) {
                _this.campLength = resp.campaigns.length;
                _this.campOpen = resp.campaigns.length;
            }
            console.log(_this.influencer);
        }, function (error) { _this.loader = 'error'; });
    };
    ;
    InfluencerComponent.prototype.updateAndUploadImageToCloud = function (fileInput, brand) {
        var _this = this;
        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var uploadData = {
                    timestamp: +new Date(),
                    api_key: '116931532498815',
                    file: e.target.result,
                    signature: CryptoJS.SHA1("timestamp=" + +new Date() + _this.cloudinaryAPISecret).toString()
                };
                _this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload', uploadData).subscribe(function (resp) {
                    brand.profile_picture_url = resp.json().secure_url;
                });
            };
            reader.readAsDataURL(fileInput.target.files[0]);
        }
        ;
    };
    ;
    InfluencerComponent.prototype.updateUser = function () {
        var _this = this;
        var user = { user_id: this.influencer.user_id, first_name: this.influencer.first_name, last_name: this.influencer.last_name, bio: this.influencer.bio };
        this.iservice.updateInf(user).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.notif.success('Success!!', resp.json().message); //sucess //heading //description
            }
            else {
                _this.notif.error('Failure!!', resp.json().message);
            }
        });
    };
    ;
    InfluencerComponent.prototype.disableOrEnableInfluencer = function () {
        var _this = this;
        this.influencer.active = !this.influencer.active;
        var user = { user_id: this.influencer.user_id, active: this.influencer.active };
        this.iservice.updateInf(this.influencer).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.iservice.getSingle(resp.json().user_id).subscribe(function (resp) {
                    _this.influencer = resp;
                    _this.enableOrDisable = resp.active === false ? 'Enable' : 'Disable';
                    _this.enableOrDisableMsg = resp.active === false ? ' User Disabled' : 'User Enable';
                    _this.notif.success('Success!!', _this.enableOrDisableMsg); //sucess //heading //description
                });
            }
            else {
                _this.notif.error('Failure!!', "");
            }
        });
    };
    ;
    InfluencerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'influencer',
            templateUrl: 'influencer-view.component.html',
            providers: [influencers_service_1.InfluencerService],
            styleUrls: ['influencer-view.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, influencers_service_1.InfluencerService, angular2_notifications_1.NotificationsService, common_1.Location, http_1.Http])
    ], InfluencerComponent);
    return InfluencerComponent;
}());
exports.InfluencerComponent = InfluencerComponent;
//# sourceMappingURL=influencer-view.component.js.map