import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Rx';
import { AuthHttp } from '../secure-http.service';

@Injectable()
export class InfluencerService  {
	private baseURL='http://instatise-server-dev.herokuapp.com/api';
	constructor(private auths:AuthHttp){}
	getAll(page:number, sort:string, value:string):Observable<any>{
		return this.auths.get(`${this.baseURL}/admin/listusers?total=10&page=${page}&sort=${sort}&role=influencer&search_key=${value}`)
				.map((resp)=>resp.json());
	}
	getSingle(id:number):Observable<any>{
		return this.auths.get(`${this.baseURL}/userdetails?user_id=${id}&campaign_limit=2&campaign_page=1`)
				.map((resp)=>resp.json());
	}
	updateInf(user:Object){
		return this.auths.post(`${this.baseURL}/updateuser`,user);
	}
}
