import { Component, OnInit ,Injectable} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {InfluencerService} from './influencers.service';
@Component({
	moduleId: module.id,
	selector: 'influencers',
	templateUrl: 'influencers.component.html',
	providers : [InfluencerService],
	styleUrls : ['influencers.component.css']
})
export class InfluencersComponent implements OnInit {
	influencers:Array<any> = [];

	influencerStatus:string='loading';
	pageStatus:string = 'loading';
    searchKey:string = '';
    sortfield:string = 'ascid';

    // Pagination variables
	influencersPerPage: number = 10;
	pageNumber:number = 1;
	totalInfluencers:number;
	totalPages:number;
	pageNumbers:Array<number>;
	pageNumberItem:any = null;

	constructor(private iservice:InfluencerService, private activatedRoute: ActivatedRoute, private router:Router) { }

	ngOnInit() {
		this.activatedRoute.queryParams.subscribe((params: Params) => {
			this.sortfield = params['sort'] || this.sortfield;
			this.pageNumber = +params['page'] || 1;
            this.searchKey = params['key'] || this.searchKey;
		});
		//get users here
		this.specificPage(this.pageNumber);
	}
	getInfluencersList(){
		this.iservice.getAll(this.pageNumber, this.sortfield, this.searchKey).subscribe(
            r =>{
                if(r.code === 611) {
                    this.pageStatus = 'notFound';
                }

                else if(r.code === 200) {
	                // console.log(r);
					// Total No. of brands
					this.totalInfluencers = r.pagination.count;
					// Set total pages and create page numbers for pagination
	            	this.totalPages = Math.ceil(this.totalInfluencers / this.influencersPerPage);
					this.pageNumbers = Array.from(new Array(this.totalPages),(val,index)=>index+1);
					
					this.influencers =r.data;
					this.influencerStatus='active';
	                this.pageStatus='active';
					for (let entry of this.influencers) {
						let fb:number=0;
						let tw:number=0;
						let yt:number=0;
						if(entry.followers){
							for (let singular of entry.followers) {
	                                singular.type === 'facebook' ? fb = singular.count :
	                                singular.type === 'twitter' ? tw = singular.count :
	                                singular.type === 'google' ? yt = singular.count : () => {};
								}
						}
						entry.fb = fb;
						entry.tw = tw;
						entry.yt = yt;
						if(!fb && !tw && !yt)
						entry.na = 'N/A'
					}

					// Page number highlight logic
                	this.pageNumberItem = "page" + this.pageNumber;
					if(this.pageNumberItem !== null) {
						setTimeout(() => { document.getElementById(this.pageNumberItem).className = 'active'; }, 0);
					}
                }
                
			},
            error => {
                this.influencerStatus = 'error';
                this.pageStatus='error';
            }
        );
	}
	sort(sorts:any) {
		this.pageNumber = 1;

		// Toggle sort order logic
		if(this.sortfield==='ascid' && sorts ==='ascid')
			sorts='dscid'
		else if(this.sortfield==='dscid'  && sorts ==='dscid')
			sorts='ascid'
		else if(this.sortfield==='ascname' && sorts ==='ascname')
			sorts='dscname'
		else if(this.sortfield==='dscname' && sorts ==='dscname')
			sorts='ascname'
		else if(this.sortfield==='ascloc' && sorts ==='ascloc')
			sorts='dscloc'
		else if(this.sortfield==='dscloc' && sorts ==='dscloc')
			sorts='ascloc'

		this.sortfield = sorts;
		this.router.navigate(['/influencers'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getInfluencersList();
	}

    onSearch(value: string){
		this.pageNumber = 1;
		this.sortfield = 'ascid';
		this.searchKey = value;
		this.pageStatus = 'loading';
		this.router.navigate(['/influencers'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.getInfluencersList();
    }

    // Previous button click handler for pagination
	previousPage(page:number) {

		this.pageNumber = page;
		this.router.navigate(['/influencers'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getInfluencersList();
		// this.pageNumber--;
	}

	// Next button click handler for pagination
	nextPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/influencers'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getInfluencersList();
		// this.pageNumber++;
	}

	// Page number li element click handler for pagination
	specificPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/influencers'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getInfluencersList();
	}


}
