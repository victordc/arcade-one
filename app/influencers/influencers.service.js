"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var secure_http_service_1 = require('../secure-http.service');
var InfluencerService = (function () {
    function InfluencerService(auths) {
        this.auths = auths;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
    }
    InfluencerService.prototype.getAll = function (page, sort, value) {
        return this.auths.get(this.baseURL + "/admin/listusers?total=10&page=" + page + "&sort=" + sort + "&role=influencer&search_key=" + value)
            .map(function (resp) { return resp.json(); });
    };
    InfluencerService.prototype.getSingle = function (id) {
        return this.auths.get(this.baseURL + "/userdetails?user_id=" + id + "&campaign_limit=2&campaign_page=1")
            .map(function (resp) { return resp.json(); });
    };
    InfluencerService.prototype.updateInf = function (user) {
        return this.auths.post(this.baseURL + "/updateuser", user);
    };
    InfluencerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [secure_http_service_1.AuthHttp])
    ], InfluencerService);
    return InfluencerService;
}());
exports.InfluencerService = InfluencerService;
//# sourceMappingURL=influencers.service.js.map