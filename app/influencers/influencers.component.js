"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var influencers_service_1 = require('./influencers.service');
var InfluencersComponent = (function () {
    function InfluencersComponent(iservice, activatedRoute, router) {
        this.iservice = iservice;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.influencers = [];
        this.influencerStatus = 'loading';
        this.pageStatus = 'loading';
        this.searchKey = '';
        this.sortfield = 'ascid';
        // Pagination variables
        this.influencersPerPage = 10;
        this.pageNumber = 1;
        this.pageNumberItem = null;
    }
    InfluencersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.sortfield = params['sort'] || _this.sortfield;
            _this.pageNumber = +params['page'] || 1;
            _this.searchKey = params['key'] || _this.searchKey;
        });
        //get users here
        this.specificPage(this.pageNumber);
    };
    InfluencersComponent.prototype.getInfluencersList = function () {
        var _this = this;
        this.iservice.getAll(this.pageNumber, this.sortfield, this.searchKey).subscribe(function (r) {
            if (r.code === 611) {
                _this.pageStatus = 'notFound';
            }
            else if (r.code === 200) {
                // console.log(r);
                // Total No. of brands
                _this.totalInfluencers = r.pagination.count;
                // Set total pages and create page numbers for pagination
                _this.totalPages = Math.ceil(_this.totalInfluencers / _this.influencersPerPage);
                _this.pageNumbers = Array.from(new Array(_this.totalPages), function (val, index) { return index + 1; });
                _this.influencers = r.data;
                _this.influencerStatus = 'active';
                _this.pageStatus = 'active';
                for (var _i = 0, _a = _this.influencers; _i < _a.length; _i++) {
                    var entry = _a[_i];
                    var fb = 0;
                    var tw = 0;
                    var yt = 0;
                    if (entry.followers) {
                        for (var _b = 0, _c = entry.followers; _b < _c.length; _b++) {
                            var singular = _c[_b];
                            singular.type === 'facebook' ? fb = singular.count :
                                singular.type === 'twitter' ? tw = singular.count :
                                    singular.type === 'google' ? yt = singular.count : function () { };
                        }
                    }
                    entry.fb = fb;
                    entry.tw = tw;
                    entry.yt = yt;
                    if (!fb && !tw && !yt)
                        entry.na = 'N/A';
                }
                // Page number highlight logic
                _this.pageNumberItem = "page" + _this.pageNumber;
                if (_this.pageNumberItem !== null) {
                    setTimeout(function () { document.getElementById(_this.pageNumberItem).className = 'active'; }, 0);
                }
            }
        }, function (error) {
            _this.influencerStatus = 'error';
            _this.pageStatus = 'error';
        });
    };
    InfluencersComponent.prototype.sort = function (sorts) {
        this.pageNumber = 1;
        // Toggle sort order logic
        if (this.sortfield === 'ascid' && sorts === 'ascid')
            sorts = 'dscid';
        else if (this.sortfield === 'dscid' && sorts === 'dscid')
            sorts = 'ascid';
        else if (this.sortfield === 'ascname' && sorts === 'ascname')
            sorts = 'dscname';
        else if (this.sortfield === 'dscname' && sorts === 'dscname')
            sorts = 'ascname';
        else if (this.sortfield === 'ascloc' && sorts === 'ascloc')
            sorts = 'dscloc';
        else if (this.sortfield === 'dscloc' && sorts === 'dscloc')
            sorts = 'ascloc';
        this.sortfield = sorts;
        this.router.navigate(['/influencers'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getInfluencersList();
    };
    InfluencersComponent.prototype.onSearch = function (value) {
        this.pageNumber = 1;
        this.sortfield = 'ascid';
        this.searchKey = value;
        this.pageStatus = 'loading';
        this.router.navigate(['/influencers'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.getInfluencersList();
    };
    // Previous button click handler for pagination
    InfluencersComponent.prototype.previousPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/influencers'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getInfluencersList();
        // this.pageNumber--;
    };
    // Next button click handler for pagination
    InfluencersComponent.prototype.nextPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/influencers'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getInfluencersList();
        // this.pageNumber++;
    };
    // Page number li element click handler for pagination
    InfluencersComponent.prototype.specificPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/influencers'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getInfluencersList();
    };
    InfluencersComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'influencers',
            templateUrl: 'influencers.component.html',
            providers: [influencers_service_1.InfluencerService],
            styleUrls: ['influencers.component.css']
        }), 
        __metadata('design:paramtypes', [influencers_service_1.InfluencerService, router_1.ActivatedRoute, router_1.Router])
    ], InfluencersComponent);
    return InfluencersComponent;
}());
exports.InfluencersComponent = InfluencersComponent;
//# sourceMappingURL=influencers.component.js.map