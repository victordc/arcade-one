"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
require('./rxjs-extensions');
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var material_1 = require('@angular/material');
var routes_1 = require('./routes');
var ng2_material_select_1 = require('ng2-material-select');
//import App Components for routes
var arcade_one_component_1 = require('./arcade-one.component');
//---------
//Authentication components
//-------------------------------
var sign_in_component_1 = require('./authentication/sign-in.component');
var logout_component_1 = require('./authentication/logout.component');
var forgot_password_component_1 = require('./authentication/forgot-password.component');
var reset_password_component_1 = require('./authentication/reset-password.component');
//--------
//Dashboard Component
//-----------------------
var dashboard_component_1 = require('./dashboard/dashboard.component');
//------------
//Campaigns Components
//------------------------
var campaigns_component_1 = require('./campaigns/campaigns.component');
var campaign_view_component_1 = require('./campaigns/campaign-view.component');
//------------
//Brnads Components
//-----------------------
var brands_component_1 = require('./brands/brands.component');
var brand_view_component_1 = require('./brands/brand-view.component');
//--------------
//Influencers Components
//-------------------------
var influencers_component_1 = require('./influencers/influencers.component');
var influencer_view_component_1 = require('./influencers/influencer-view.component');
//--------
//Payments Components
//-----------------------
var payments_component_1 = require('./payments/payments.component');
var payment_view_component_1 = require('./payments/payment-view.component');
var configure_paypal_component_1 = require('./payments/configure-paypal.component');
//--------
//User Components
//-----------------------
var users_component_1 = require('./users/users.component');
var user_view_component_1 = require('./users/user-view.component');
var user_form_component_1 = require('./users/user-form.component');
//--------
//User Components
//-----------------------
var authentication_service_1 = require('./authentication/service/authentication.service');
var angular2_notifications_1 = require('angular2-notifications');
var authentication_guard_service_1 = require('./authentication/authentication-guard.service');
var secure_http_service_1 = require('./secure-http.service');
//----
// pipes
//----------
var readablenumber_pipe_1 = require('./helpers/readablenumber-pipe');
var capitalize_pipe_1 = require('./helpers/capitalize.pipe');
var ellipsis_pipe_1 = require('./helpers/ellipsis.pipe');
var ArcadeOneModule = (function () {
    function ArcadeOneModule() {
    }
    ArcadeOneModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                http_1.JsonpModule,
                routes_1.ArcadeOneRoutingModule,
                angular2_notifications_1.SimpleNotificationsModule,
                material_1.MaterialModule.forRoot(),
                ng2_material_select_1.Ng2SelectModule
            ],
            declarations: [
                arcade_one_component_1.ArcadeOneComponent,
                sign_in_component_1.SignInComponent,
                logout_component_1.LogoutComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                reset_password_component_1.ResetPasswordComponent,
                dashboard_component_1.DashboardComponent,
                campaigns_component_1.CampaignsComponent,
                campaign_view_component_1.CampaignComponent,
                brands_component_1.BrandsComponent,
                brand_view_component_1.BrandComponent,
                influencers_component_1.InfluencersComponent,
                influencer_view_component_1.InfluencerComponent,
                payments_component_1.PaymentsComponent,
                payment_view_component_1.PaymentComponent,
                configure_paypal_component_1.ConfigurePaypalComponent,
                users_component_1.UsersComponent,
                user_view_component_1.UserComponent,
                user_form_component_1.UserFormComponent,
                readablenumber_pipe_1.ExponentialStrengthPipe,
                capitalize_pipe_1.CapitalizePipe,
                ellipsis_pipe_1.TruncatePipe
            ],
            providers: [
                authentication_service_1.AuthService, authentication_guard_service_1.AuthenticationGuard, secure_http_service_1.AUTH_PROVIDERS
            ],
            bootstrap: [arcade_one_component_1.ArcadeOneComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ArcadeOneModule);
    return ArcadeOneModule;
}());
exports.ArcadeOneModule = ArcadeOneModule;
/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
//# sourceMappingURL=arcade-one.module.js.map