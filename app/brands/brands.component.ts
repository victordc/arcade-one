import { Component, OnInit,trigger, transition, style, animate, state } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Injectable } from '@angular/core';
import { BrandService } from '../brands/brand.service';

@Component({
	moduleId: module.id,
	selector: 'brands',
	animations: [
	    trigger(
	      'myAnimation', [
	        transition(':enter', [
	          style({opacity: 0}),
	          animate('100ms', style({opacity: 1}))
	        ]),
	        transition(':leave', [
	          style({opacity: 1}),
	          animate('100ms', style({opacity: 0}))
	        ])
	      ]
	    )
	],
	templateUrl: 'brands.component.html',
	styleUrls:['brands.component.css'],
	providers:[Http, BrandService]

})

export class BrandsComponent implements OnInit {

	brands:Array<any>;
    entities:boolean = false;
    brandStatus:string='loading';
    pageStatus:string='loading';
	sortfield:string = 'ascid';
	searchKey:string= '';

	// Pagination variables
	brandsPerPage: number = 10;
	pageNumber:number = 1;
	totalBrands:number;
	totalPages:number;
	pageNumbers:Array<number>;
	pageNumberItem:any = null;

	constructor(private brand: BrandService, private activatedRoute: ActivatedRoute, private router:Router) { }

	ngOnInit() {

		this.activatedRoute.queryParams.subscribe((params: Params) => {
			this.sortfield = params['sort'] || this.sortfield;
			this.pageNumber = +params['page'] || 1;
            this.searchKey = params['key'] || this.searchKey;
		});
		//get users here
		this.specificPage(this.pageNumber);
	}

	getBrandsList(){
		this.brand.fetchBrands(this.pageNumber, this.sortfield, this.searchKey).subscribe(
            r => {
                if(r.json().code === 611){
                    this.pageStatus = 'notFound';
                }else if(r.json().code === 200){
                    // console.log(r.json());
    				// Total No. of brands
    				this.totalBrands = r.json().pagination.count;
    				// Set total pages and create page numbers for pagination
                	this.totalPages = Math.ceil(this.totalBrands / this.brandsPerPage);
    				this.pageNumbers = Array.from(new Array(this.totalPages),(val,index)=>index+1);

    				this.brands = r.json().data;
                    this.brandStatus = 'active';
                    this.pageStatus = 'active';
    				this.entities = true;

    				// Page number highlight logic
                	this.pageNumberItem = "page" + this.pageNumber;
                	console.log(this.pageNumber);
                	console.log(this.pageNumberItem);
					if(this.pageNumberItem !== null) {
						setTimeout(() => { document.getElementById(this.pageNumberItem).className = 'active'; }, 0);
					}
                }

			},
            error => {
                this.brandStatus = 'error';
                this.pageStatus = 'error';
            }
		);
	}

	sort(sorts:any){
		this.pageNumber = 1;

		// Toggle sort order logic
		if(this.sortfield==='ascid' && sorts ==='ascid')
			sorts='dscid'
		else if(this.sortfield==='dscid'  && sorts ==='dscid')
			sorts='ascid'
		else if(this.sortfield==='ascname' && sorts ==='ascname')
			sorts='dscname'
		else if(this.sortfield==='dscname' && sorts ==='dscname')
			sorts='ascname'
		else if(this.sortfield==='ascemail' && sorts ==='ascemail')
			sorts='dscemail'
		else if(this.sortfield==='dscemail' && sorts ==='dscemail')
			sorts='ascemail'

		this.sortfield = sorts;
		this.router.navigate(['/brands'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getBrandsList();
	}

    onSearch(value: string){
		this.pageNumber = 1;
		this.sortfield = 'ascid';
		this.searchKey = value;
		this.pageStatus = 'loading';
		this.router.navigate(['/brands'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.getBrandsList();
    }

    // Previous button click handler for pagination
	previousPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/brands'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getBrandsList();
		// this.pageNumber--;
	}

	// Next button click handler for pagination
	nextPage(page:number) {
		console.log(page);
		this.pageNumber = page;
		this.router.navigate(['/brands'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getBrandsList();
		// this.pageNumber++;
		console.log(this.pageNumber);
	}

	// Page number li element click handler for pagination
	specificPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/brands'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getBrandsList();
	}


}
