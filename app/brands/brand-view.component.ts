import { Component, OnInit,trigger, transition, style, animate, state, ElementRef, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { BrandService } from '../brands/brand.service';
import { BrandModel} from '../brands/brand.model';
import { Location } from '@angular/common';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Http, HttpModule } from '@angular/http';
import * as CryptoJS from 'crypto-js';

@Component({
	moduleId: module.id,
	selector: 'brand',
	templateUrl: 'brand-view.component.html',
    animations: [
	    trigger(
	      'myAnimation', [
	        transition(':enter', [
	          style({opacity: 0}),
	          animate('100ms', style({opacity: 1}))
	        ]),
	        transition(':leave', [
	          style({opacity: 1}),
	          animate('100ms', style({opacity: 0}))
	        ])
	      ]
	    )
	],
	providers:[BrandService,BrandModel],
	styleUrls:['brand-view.component.css']
})
export class BrandComponent implements OnInit {

    brand:BrandModel = new BrandModel();
    campaigns:Array<any>;
		currentState:string;
		entities:boolean = false;
		enableOrDisable:string;
		enableOrDisableMsg:string;
    cloudinaryAPISecret:string ='zwZBoDciK30vckcAlhcYV2kodSU';
    loader:string='loading';
    pageStatus:string='loading';
    submitted:boolean = false;

	constructor(private brandService: BrandService,
							private notif:NotificationsService,
              private element: ElementRef,
							private location:Location,
							private http:Http) {}

	ngOnInit() {
        let idOfBrand = +location.pathname.split('/')[2];
        //get brand details
        this.brandService.fetchBrand(idOfBrand).subscribe(
            r => {
                this.brand = r.json();
                this.campaigns = this.brand.campaigns.hasOwnProperty('data') ? this.brand.campaigns.data : [];
                this.loader='active';
                this.pageStatus = 'active';
                this.enableOrDisable = r.json().active === false ? 'enable' : 'disable';
                this.entities = true;
            },
            error => {
                this.loader = 'error';
                this.pageStatus = 'error';
            }
        );
    }

    public updateAndUploadImageToCloud(fileInput: any, brand:any){

           if (fileInput.target.files && fileInput.target.files[0]) {
               this.pageStatus = 'loading';
                var reader = new FileReader();
                reader.onload =  (e : any) => {

                    let uploadData:Object = {
                        timestamp: +new Date(),
                        api_key:'116931532498815',
                        file:e.target.result,
                        signature:CryptoJS.SHA1(`timestamp=${+new Date()}${this.cloudinaryAPISecret}`).toString()
                    };

                    this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload',uploadData).subscribe(
                        r => {
                            brand.profile_picture_url = r.json().secure_url;
                            uploadData = {};
                            this.pageStatus = 'active';
                        },
                        error => {
                            this.notif.error('Failure!!',`Try uploading less size`);
                            this.pageStatus = 'active';
                        }
                    );
                }
                reader.readAsDataURL(fileInput.target.files[0]);
            };

    }

    public updateBrand(brand:any){
        this.pageStatus = 'loading';
        this.submitted = true;
        this.brandService.updateBrand(brand).subscribe(
					r => {
            if(r.json().code === 200 ){
							this.notif.success('Success!!',r.json().message);//sucess //heading //description
              this.pageStatus = 'active';
						}else{
							this.notif.error('Failure!!',r.json().message);
              this.pageStatus = 'error';
						}
        });
    }

    public disableOrEnablebrand(brand:any){
        this.pageStatus = 'loading';
				brand.active = !brand.active;
				this.brandService.updateBrand(brand).subscribe(
					r => {
						if(r.json().code === 200 ){
							this.brandService.fetchBrand(r.json().user_id).subscribe(
								r => {
									this.brand = r.json();
									this.enableOrDisable = r.json().active === false ? 'enable' : 'disable';
									this.enableOrDisableMsg = r.json().active === false ? ' User disabled' : 'User enabled';
									this.notif.success('Success!!',this.enableOrDisableMsg);
                  this.pageStatus = 'active';
							});
						}else{
							this.notif.error('Failure!!',"");
              this.pageStatus = 'error';
						}
					})
		};


}
