import { Injectable } from '@angular/core';
import { AuthHttp } from '../secure-http.service';

@Injectable()

export class BrandService {

	baseURL='http://instatise-server-dev.herokuapp.com/api';

	constructor(private auth:AuthHttp) {}

	fetchBrands(page:number, sort:string, value:string){
		return this.auth.get(`${this.baseURL}/admin/listusers?total=10&page=${page}&sort=${sort}&role=brand&search_key=${value}`);
	}
	fetchBrand(id:number){
		return this.auth.get(`${this.baseURL}/userdetails?user_id=${id}&campaign_limit=5&campaign_page=1`);
	}
	updateBrand(brand:Object){
		return this.auth.post(`${this.baseURL}/updateuser`,brand);
	}
}
