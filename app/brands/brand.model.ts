export class BrandModel {

      brand_id: number;
      brand_name: string;
      bio: null;
      profile_picture_url: string;
      total_campaigns: number;
      completed_campaigns: number;
      open_campaigns: number;
      social_data:Array<any>;
      active: boolean;
      created: string;
      campaigns: any;
}
