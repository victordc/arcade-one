"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var brand_service_1 = require('../brands/brand.service');
var brand_model_1 = require('../brands/brand.model');
var common_1 = require('@angular/common');
var angular2_notifications_1 = require('angular2-notifications');
var http_1 = require('@angular/http');
var CryptoJS = require('crypto-js');
var BrandComponent = (function () {
    function BrandComponent(brandService, notif, element, location, http) {
        this.brandService = brandService;
        this.notif = notif;
        this.element = element;
        this.location = location;
        this.http = http;
        this.brand = new brand_model_1.BrandModel();
        this.entities = false;
        this.cloudinaryAPISecret = 'zwZBoDciK30vckcAlhcYV2kodSU';
        this.loader = 'loading';
    }
    BrandComponent.prototype.ngOnInit = function () {
        var _this = this;
        var idOfBrand = +location.pathname.split('/')[2];
        //get brand details
        this.brandService.fetchBrand(idOfBrand).subscribe(function (resp) {
            _this.brand = resp.json();
            _this.campaigns = _this.brand.campaigns.hasOwnProperty('data') ? _this.brand.campaigns.data : [];
            _this.loader = 'active';
            _this.enableOrDisable = resp.json().active === false ? 'enable' : 'disable';
            _this.entities = true;
        }, function (error) { _this.loader = 'error'; });
    };
    BrandComponent.prototype.updateAndUploadImageToCloud = function (fileInput, brand) {
        var _this = this;
        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var uploadData = {
                    timestamp: +new Date(),
                    api_key: '116931532498815',
                    file: e.target.result,
                    signature: CryptoJS.SHA1("timestamp=" + +new Date() + _this.cloudinaryAPISecret).toString()
                };
                _this.http.post('https://api.cloudinary.com/v1_1/dl2z7s9ol/image/upload', uploadData).subscribe(function (resp) {
                    brand.profile_picture_url = resp.json().secure_url;
                    uploadData = {};
                });
            };
            reader.readAsDataURL(fileInput.target.files[0]);
        }
        ;
    };
    BrandComponent.prototype.updateBrand = function (brand) {
        var _this = this;
        this.brandService.updateBrand(brand).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.notif.success('Success!!', resp.json().message); //sucess //heading //description
            }
            else {
                _this.notif.error('Failure!!', resp.json().message);
            }
        });
    };
    BrandComponent.prototype.disableOrEnablebrand = function (brand) {
        var _this = this;
        brand.active = !brand.active;
        this.brandService.updateBrand(brand).subscribe(function (resp) {
            if (resp.json().code === 200) {
                _this.brandService.fetchBrand(resp.json().user_id).subscribe(function (resp) {
                    _this.brand = resp.json();
                    _this.enableOrDisable = resp.json().active === false ? 'enable' : 'disable';
                    _this.enableOrDisableMsg = resp.json().active === false ? ' User disabled' : 'User enabled';
                    _this.notif.success('Success!!', _this.enableOrDisableMsg); //sucess //heading //description
                });
            }
            else {
                _this.notif.error('Failure!!', "");
            }
        });
    };
    ;
    BrandComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'brand',
            templateUrl: 'brand.component.html',
            animations: [
                core_1.trigger('myAnimation', [
                    core_1.transition(':enter', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate('100ms', core_1.style({ opacity: 1 }))
                    ]),
                    core_1.transition(':leave', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate('100ms', core_1.style({ opacity: 0 }))
                    ])
                ])
            ],
            providers: [brand_service_1.BrandService, brand_model_1.BrandModel],
            styleUrls: ['brand.component.css']
        }), 
        __metadata('design:paramtypes', [brand_service_1.BrandService, angular2_notifications_1.NotificationsService, core_1.ElementRef, common_1.Location, http_1.Http])
    ], BrandComponent);
    return BrandComponent;
}());
exports.BrandComponent = BrandComponent;
//# sourceMappingURL=brand.component.js.map