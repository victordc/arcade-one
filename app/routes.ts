//property of dietcode

//import ANGULAR || BROWSER || ES6 || CUSTOM  modules here
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//import App Components for routes
//---------
//Authentication components
//-------------------------------
import { SignInComponent }  from './authentication/sign-in.component';
import { LogoutComponent }  from './authentication/logout.component';
import { ForgotPasswordComponent }  from './authentication/forgot-password.component';
import { ResetPasswordComponent }  from './authentication/reset-password.component';
//--------
//Dashboard Component
//-----------------------
import { DashboardComponent }  from './dashboard/dashboard.component';
//------------
//Campaigns Components
//------------------------
import { CampaignsComponent }  from './campaigns/campaigns.component';
import { CampaignComponent }  from './campaigns/campaign-view.component';
//------------
//Brnads Components
//-----------------------
import { BrandsComponent }  from './brands/brands.component';
import { BrandComponent }  from './brands/brand-view.component';
//--------------
//Influencers Components
//-------------------------
import { InfluencersComponent }  from './influencers/influencers.component';
import { InfluencerComponent }  from './influencers/influencer-view.component';
//--------
//Payments Components
//-----------------------
import {  PaymentsComponent } from './payments/payments.component';
import { PaymentComponent }   from './payments/payment-view.component';
import { ConfigurePaypalComponent } from './payments/configure-paypal.component';
//--------
//User Components
//-----------------------
import { UserComponent }   from './users/user-view.component';
import { UserFormComponent }  from './users/user-form.component';
import { UsersComponent }  from './users/users.component';


import { AuthenticationGuard } from './authentication/authentication-guard.service';
import { RedirectIfLoggedIn } from './authentication/redirect-loggedin-guard.service';

const routes: Routes = [
  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '',  component: SignInComponent,canActivate:[RedirectIfLoggedIn] },
  { path: 'login',  component: SignInComponent,canActivate:[RedirectIfLoggedIn]  },
  { path: 'logout',  component: LogoutComponent  },
  { path: 'forgot',  component: ForgotPasswordComponent},
  { path: 'change-password',  component: ResetPasswordComponent },
  { path: 'dashboard',  component: DashboardComponent ,canActivate:[AuthenticationGuard]},
  { path: 'campaigns',  component: CampaignsComponent ,canActivate:[AuthenticationGuard]},
  { path: 'campaigns/:id',  component: CampaignComponent ,canActivate:[AuthenticationGuard]},
  { path: 'brands',  component: BrandsComponent ,canActivate:[AuthenticationGuard]},
  { path: 'brands/:id',  component: BrandComponent ,canActivate:[AuthenticationGuard]},
  { path: 'influencers',  component: InfluencersComponent ,canActivate:[AuthenticationGuard]},
  { path: 'influencers/:id',  component: InfluencerComponent ,canActivate:[AuthenticationGuard]},
  { path: 'payments', component: PaymentsComponent ,canActivate:[AuthenticationGuard]},
  { path: 'payments/:id',  component: PaymentComponent ,canActivate:[AuthenticationGuard]},
  { path: 'configure-paypal',  component: ConfigurePaypalComponent ,canActivate:[AuthenticationGuard]},
  { path: 'users', component: UsersComponent ,canActivate:[AuthenticationGuard]},
  { path: 'users/add', component: UserFormComponent ,canActivate:[AuthenticationGuard]},
  { path: 'users/:id', component: UserComponent ,canActivate:[AuthenticationGuard]},

  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers : [AuthenticationGuard,RedirectIfLoggedIn]
})
export class ArcadeOneRoutingModule { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
