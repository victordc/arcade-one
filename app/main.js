"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var arcade_one_module_1 = require('./arcade-one.module');
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(arcade_one_module_1.ArcadeOneModule);
/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
//# sourceMappingURL=main.js.map