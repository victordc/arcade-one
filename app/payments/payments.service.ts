import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Rx';
import { AuthHttp } from '../secure-http.service';

@Injectable()
export class PaymentsService  {
	private baseURL='http://instatise-server-dev.herokuapp.com/api';
	constructor(private auths:AuthHttp){}
	getAll(page:number, sort:string, value:string, filter:string):Observable<any>{
		return this.auths.get(`${this.baseURL}/admin/listpayments?sort=${sort}&total=10&page=${page}&filter=${filter}&search_key=${value}`)
				.map((resp)=>resp.json());
	}
	getSingle(id:number):Observable<any>{
		return this.auths.get(`${this.baseURL}/admin/getpayments?campaign_id=${id}`)
	}
	updateInf(user:Object){
		return this.auths.post(`${this.baseURL}/updateuser`,user);
	}
}
