"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_notifications_1 = require('angular2-notifications');
var common_1 = require('@angular/common');
var secure_http_service_1 = require('../secure-http.service');
var core_2 = require("@angular/core");
var ConfigurePaypalComponent = (function () {
    function ConfigurePaypalComponent(notif, location, auth) {
        this.notif = notif;
        this.location = location;
        this.auth = auth;
        this.paypal_email = '';
        this.emailParadigm = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
        this.entities = false;
        this.loader = 'loading';
        this.pageStatus = 'loading';
        this.submitted = false;
    }
    ConfigurePaypalComponent.prototype.ngOnInit = function () {
        this.entities = true;
        this.loader = 'active';
        this.pageStatus = 'active';
    };
    ConfigurePaypalComponent.prototype.configurePaypal = function (email) {
        var _this = this;
        this.pageStatus = 'loading';
        this.submitted = true;
        this.auth.post(this.baseURL + "/configure/add_edit", { config_key: 'paypal_email', config_value: email }).subscribe(function (r) {
            _this.notif.success('Success!!', "Paypal email configured!");
            _this.pageStatus = 'active';
        }, function (err) {
            _this.notif.error('Failure!!', "Error occured while configuring!");
            _this.pageStatus = 'active';
        });
    };
    ConfigurePaypalComponent = __decorate([
        core_2.Injectable(),
        core_1.Component({
            moduleId: module.id,
            selector: 'configure-paypal',
            animations: [
                core_1.trigger('myAnimation', [
                    core_1.transition(':enter', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate('100ms', core_1.style({ opacity: 1 }))
                    ]),
                    core_1.transition(':leave', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate('100ms', core_1.style({ opacity: 0 }))
                    ])
                ])
            ],
            templateUrl: 'configure-paypal.component.html',
            styleUrls: ['configure-paypal.component.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [angular2_notifications_1.NotificationsService, common_1.Location, secure_http_service_1.AuthHttp])
    ], ConfigurePaypalComponent);
    return ConfigurePaypalComponent;
}());
exports.ConfigurePaypalComponent = ConfigurePaypalComponent;
//# sourceMappingURL=configure-paypal.component.js.map