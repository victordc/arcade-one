"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var payments_service_1 = require('./payments.service');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
require('rxjs/add/operator/switchMap');
var PaymentsComponent = (function () {
    function PaymentsComponent(paymentsService, location, router, activatedRoute) {
        this.paymentsService = paymentsService;
        this.location = location;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.paymentsType = [];
        this.paymentStatus = 'loading';
        this.pageStatus = 'loading';
        this.searchKey = '';
        this.sortfield = 'asccrt';
        // Pagination variables
        this.paymentsPerPage = 10;
        this.pageNumber = 1;
        this.location = location;
    }
    PaymentsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paymentTypeValue = { name: 'All', value: '' };
        this.paymentsType = [
            { name: 'All', value: '' },
            { name: 'Failed', value: 'failed' },
            { name: 'Pending', value: 'pending' },
            { name: 'Refunded', value: 'refunded' }
        ];
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.sortfield = params['sort'] || _this.sortfield;
            _this.searchKey = params['key'] || _this.searchKey;
        });
        //get users here
        this.specificPage(this.pageNumber);
    };
    PaymentsComponent.prototype.selected = function () {
        console.log(111);
        this.getPaymentList();
    };
    PaymentsComponent.prototype.getPaymentList = function () {
        var _this = this;
        if (!this.sortfield) {
            this.sortfield = 'asccrt';
        }
        this.paymentsService.getAll(this.pageNumber, this.sortfield, this.searchKey, (this.paymentTypeValue.value || ''))
            .subscribe(function (r) {
            if (r.code === 611) {
                _this.pageStatus = 'notFound';
            }
            else if (r.code === 200) {
                // console.log(r);
                // Total No. of brands
                _this.totalPayments = r.pagination.count;
                // Set total pages and create page numbers for pagination
                _this.totalPages = Math.ceil(_this.totalPayments / _this.paymentsPerPage);
                _this.pageNumbers = Array.from(new Array(_this.totalPages), function (val, index) { return index + 1; });
                _this.payments = r.data;
                // console.log(r.data)
                _this.paymentStatus = 'active';
                _this.pageStatus = 'active';
            }
        }, function (error) {
            _this.paymentStatus = 'error';
            _this.pageStatus = 'error';
        });
    };
    PaymentsComponent.prototype.sort = function (sorts) {
        this.pageNumber = 1;
        // Toggle sort order logic
        if (this.sortfield === 'asccrt' && sorts === 'asccrt')
            sorts = 'dsccrt';
        else if (this.sortfield === 'dsccrt' && sorts === 'dsccrt')
            sorts = 'asccrt';
        else if (this.sortfield === 'asccname' && sorts === 'asccname')
            sorts = 'dsccname';
        else if (this.sortfield === 'dsccname' && sorts === 'dsccname')
            sorts = 'asccname';
        else if (this.sortfield === 'ascbname' && sorts === 'ascbname')
            sorts = 'dscbname';
        else if (this.sortfield === 'dscbname' && sorts === 'dscbname')
            sorts = 'ascbname';
        else if (this.sortfield === 'asciname' && sorts === 'asciname')
            sorts = 'dsciname';
        else if (this.sortfield === 'dsciname' && sorts === 'dsciname')
            sorts = 'asciname';
        this.sortfield = sorts;
        this.router.navigate(['/payments'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getPaymentList();
    };
    PaymentsComponent.prototype.onSearch = function (value) {
        this.pageNumber = 1;
        this.sortfield = 'asccrt';
        this.searchKey = value;
        this.pageStatus = 'loading';
        this.router.navigate(['/payments'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.getPaymentList();
    };
    // Previous button click handler for pagination
    PaymentsComponent.prototype.previousPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/payments'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getPaymentList();
        this.pageNumber--;
    };
    // Next button click handler for pagination
    PaymentsComponent.prototype.nextPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/payments'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getPaymentList();
        this.pageNumber++;
    };
    // Page number li element click handler for pagination
    PaymentsComponent.prototype.specificPage = function (page) {
        this.pageNumber = page;
        this.router.navigate(['/payments'], { queryParams: { page: this.pageNumber, sort: this.sortfield, key: this.searchKey } });
        this.pageStatus = 'loading';
        this.getPaymentList();
    };
    PaymentsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'payments',
            templateUrl: 'payments.component.html',
            providers: [payments_service_1.PaymentsService],
            styleUrls: ['payments.component.css']
        }), 
        __metadata('design:paramtypes', [payments_service_1.PaymentsService, common_1.Location, router_1.Router, router_1.ActivatedRoute])
    ], PaymentsComponent);
    return PaymentsComponent;
}());
exports.PaymentsComponent = PaymentsComponent;
//# sourceMappingURL=payments.component.js.map