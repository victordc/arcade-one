import { Component, OnInit } from '@angular/core';
import { PaymentsService } from './payments.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
	moduleId: module.id,
	selector: 'payments',
	templateUrl: 'payments.component.html',
	providers : [PaymentsService],
	styleUrls : ['payments.component.css']
})
export class PaymentsComponent implements OnInit {
	payments:any;
	paymentTypeValue:any;
	paymentsType:Array<any> = [];
	paymentStatus:string='loading';
	pageStatus:string = 'loading';
    searchKey:string = '';
    sortfield:string = 'asccrt';

    // Pagination variables
	paymentsPerPage: number = 10;
	pageNumber:number = 1;
	totalPayments:number;
	totalPages:number;
	pageNumbers:Array<number>;
	constructor(private paymentsService:PaymentsService, private location:Location , private router:Router, private activatedRoute: ActivatedRoute) {
		this.location = location;
	}

    ngOnInit() {
		this.paymentTypeValue={name:'All',value:''};
		this.paymentsType = [
			{name:'All',value:''},
			{name:'Failed',value:'failed'},
			{name:'Pending',value:'pending'},
			{name:'Refunded',value:'refunded'}
		]
		this.activatedRoute.queryParams.subscribe((params: Params) => {
			this.sortfield = params['sort'] || this.sortfield;
            this.searchKey = params['key'] || this.searchKey;
		});
		//get users here
		this.specificPage(this.pageNumber);
    }
	selected(){
			console.log(111)
			this.getPaymentList();
	}
	getPaymentList(){
		if(!this.sortfield){
			this.sortfield='asccrt';
		}
		this.paymentsService.getAll(this.pageNumber, this.sortfield, this.searchKey, (this.paymentTypeValue.value || ''))
				.subscribe(
            r =>{
              if(r.code === 611) {
                this.pageStatus = 'notFound';
              }
              else if(r.code === 200) {
                // console.log(r);
								// Total No. of brands
								this.totalPayments = r.pagination.count;
								// Set total pages and create page numbers for pagination
            		this.totalPages = Math.ceil(this.totalPayments / this.paymentsPerPage);
								this.pageNumbers = Array.from(new Array(this.totalPages),(val,index)=>index+1);
								this.payments =r.data;
								// console.log(r.data)
								this.paymentStatus='active';
                this.pageStatus='active';
              }
						},
            error => {
                this.paymentStatus = 'error';
                this.pageStatus='error';
            }
        );
		}
	sort(sorts:any) {
		this.pageNumber = 1;

		// Toggle sort order logic
		if(this.sortfield==='asccrt' && sorts ==='asccrt')
			sorts='dsccrt'
		else if(this.sortfield==='dsccrt'  && sorts ==='dsccrt')
			sorts='asccrt'
		else if(this.sortfield==='asccname' && sorts ==='asccname')
			sorts='dsccname'
		else if(this.sortfield==='dsccname' && sorts ==='dsccname')
			sorts='asccname'
		else if(this.sortfield==='ascbname' && sorts ==='ascbname')
			sorts='dscbname'
		else if(this.sortfield==='dscbname' && sorts ==='dscbname')
			sorts='ascbname'
		else if(this.sortfield==='asciname' && sorts ==='asciname')
			sorts='dsciname'
		else if(this.sortfield==='dsciname' && sorts ==='dsciname')
			sorts='asciname'

		this.sortfield = sorts;
		this.router.navigate(['/payments'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getPaymentList();
	}
	onSearch(value: string){
		this.pageNumber = 1;
		this.sortfield = 'asccrt';
		this.searchKey = value;
		this.pageStatus = 'loading';
		this.router.navigate(['/payments'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.getPaymentList();
    }
	// Previous button click handler for pagination
	previousPage(page:number) {

		this.pageNumber = page;
		this.router.navigate(['/payments'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getPaymentList();
		this.pageNumber--;
	}

	// Next button click handler for pagination
	nextPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/payments'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getPaymentList();
		this.pageNumber++;
	}

	// Page number li element click handler for pagination
	specificPage(page:number) {
		this.pageNumber = page;
		this.router.navigate(['/payments'], { queryParams: {page: this.pageNumber, sort:this.sortfield, key:this.searchKey} });
		this.pageStatus = 'loading';
		this.getPaymentList();
	}
}
