import { Component, OnInit,trigger, transition, style, animate, state } from '@angular/core';
import { PaymentsService } from './payments.service';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Location } from '@angular/common';


@Component({
    moduleId:module.id,
    selector: 'payment-view',
    animations: [
	    trigger(
	      'myAnimation', [
	        transition(':enter', [
	          style({opacity: 0}),
	          animate('100ms', style({opacity: 1}))
	        ]),
	        transition(':leave', [
	          style({opacity: 1}),
	          animate('100ms', style({opacity: 0}))
	        ])
	      ]
	    )
	],
    templateUrl: 'payment-view.component.html',
    styleUrls:['payment-view.component.css'],
    providers:[PaymentsService]
})

export class PaymentComponent implements OnInit {

    payment:any;
    entities:boolean = false;
    loader:string='loading';
    pageStatus:string='loading';

    constructor(private paymentService:PaymentsService,
                private notif:NotificationsService,
                private location:Location) {}

    ngOnInit() {

        let idOfPayment = +location.pathname.split('/')[2];
        //get payment details
        this.paymentService.getSingle(idOfPayment).subscribe(
            r => {
                this.payment = r.json().data;
                console.log(this.payment);
                this.loader='active';
                this.pageStatus = 'active';
                this.entities = true;
            },
            error => {
                this.loader = 'error';
                this.pageStatus = 'error';
            }
        );
    };

}
