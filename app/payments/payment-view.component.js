"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var payments_service_1 = require('./payments.service');
var angular2_notifications_1 = require('angular2-notifications');
var common_1 = require('@angular/common');
var PaymentComponent = (function () {
    function PaymentComponent(paymentService, notif, location) {
        this.paymentService = paymentService;
        this.notif = notif;
        this.location = location;
        this.entities = false;
        this.loader = 'loading';
        this.pageStatus = 'loading';
    }
    PaymentComponent.prototype.ngOnInit = function () {
        var _this = this;
        var idOfPayment = +location.pathname.split('/')[2];
        //get payment details
        this.paymentService.getSingle(idOfPayment).subscribe(function (r) {
            _this.payment = r.json().data;
            console.log(_this.payment);
            _this.loader = 'active';
            _this.pageStatus = 'active';
            _this.entities = true;
        }, function (error) {
            _this.loader = 'error';
            _this.pageStatus = 'error';
        });
    };
    ;
    PaymentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'payment-view',
            animations: [
                core_1.trigger('myAnimation', [
                    core_1.transition(':enter', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate('100ms', core_1.style({ opacity: 1 }))
                    ]),
                    core_1.transition(':leave', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate('100ms', core_1.style({ opacity: 0 }))
                    ])
                ])
            ],
            templateUrl: 'payment-view.component.html',
            styleUrls: ['payment-view.component.css'],
            providers: [payments_service_1.PaymentsService]
        }), 
        __metadata('design:paramtypes', [payments_service_1.PaymentsService, angular2_notifications_1.NotificationsService, common_1.Location])
    ], PaymentComponent);
    return PaymentComponent;
}());
exports.PaymentComponent = PaymentComponent;
//# sourceMappingURL=payment-view.component.js.map