import { Component, OnInit,trigger, transition, style, animate, state } from '@angular/core';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Location } from '@angular/common';
import { AuthHttp } from '../secure-http.service';
import { Injectable } from "@angular/core";

@Injectable()

@Component({
  moduleId:module.id,
  selector: 'configure-paypal',
  animations: [
      trigger(
        'myAnimation', [
          transition(':enter', [
            style({opacity: 0}),
            animate('100ms', style({opacity: 1}))
          ]),
          transition(':leave', [
            style({opacity: 1}),
            animate('100ms', style({opacity: 0}))
          ])
        ]
      )
  ],
  templateUrl: 'configure-paypal.component.html',
  styleUrls:['configure-paypal.component.css'],
  providers:[]
})
export class ConfigurePaypalComponent implements OnInit {

    paypal_email:string='';
    emailParadigm = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    baseURL='http://instatise-server-dev.herokuapp.com/api';
	entities:boolean = false;
	loader:string='loading';
    pageStatus:string='loading';
    submitted = false;

  constructor(private notif:NotificationsService,private location:Location, private auth:AuthHttp) {  }

  ngOnInit() {
      this.entities = true;
      this.loader='active';
      this.pageStatus ='active';
  }

  configurePaypal(email:string){
      this.pageStatus = 'loading';
      this.submitted = true;
      this.auth.post(`${this.baseURL}/configure/add_edit`,{config_key:'paypal_email',config_value:email}).subscribe(
          r => {
              this.notif.success('Success!!',`Paypal email configured!`);
              this.pageStatus = 'active';
          },
          err => {
              this.notif.error('Failure!!',`Error occured while configuring!`);
              this.pageStatus = 'active';
          }
      )
  }

}
