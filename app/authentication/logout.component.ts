import { Component, OnInit } from '@angular/core';
import {Injectable} from '@angular/core';
import {AuthService} from './service/authentication.service';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { AuthHttp } from '../secure-http.service';
import {Router} from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'logout',
	template :``
})
@Injectable()
export class LogoutComponent implements OnInit {
	constructor(private router:Router,private notif:NotificationsService) { 

	}
	ngOnInit() { 
		this.logout();
	}
	logout(){
		localStorage.removeItem('id_token');
		this.router.navigate(['login']);
		this.notif.success('Successfully logged out','');
	}
	
}