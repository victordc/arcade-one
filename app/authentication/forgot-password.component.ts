import { Component, OnInit,Injectable } from '@angular/core';
import {AuthService} from './service/authentication.service';  
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';
import { Router } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'forgot-password',
	templateUrl: 'forgot-password.component.html',
	styleUrls:['forgot-password.component.css']

})
@Injectable()
export class ForgotPasswordComponent implements OnInit {
	email:String;
	constructor(private auths:AuthService ,private notif:NotificationsService,private router:Router) { }
	resetPassword(){
		const hostname=location.href.split('/')[0]+'//'+location.href.split('//')[1].split('/')[0];
		this.auths.forgotPassword({
			email : this.email,
			domain : hostname
		}).subscribe((resp)=>{
			if(typeof resp==='object' && resp.code===611){
				this.notif.error('Error',resp.message);
			}else{
				this.notif.success('Success','An e-mail with reset link has been sent to '+this.email);
				this.router.navigate(['/login']);
			}
		},(error)=>{
				this.notif.error('Error','Some Error Occurred');
		})
	}
	ngOnInit() { }

	
}