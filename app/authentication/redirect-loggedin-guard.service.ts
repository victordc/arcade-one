import { Injectable } from '@angular/core';
import { RouterModule, Routes,Router,ActivatedRouteSnapshot,RouterStateSnapshot,CanActivate } from '@angular/router';

@Injectable()
export class RedirectIfLoggedIn implements CanActivate{
	constructor(private router:Router){
		
	}
	getExpiration(token:String):boolean{
		let hash=token.split('.')[1];
		let obj=JSON.parse(atob(hash));
		let current=new Date().getTime();
		let expiration=obj.exp*1000;
		return (expiration > current);
	}
	canActivate(next:ActivatedRouteSnapshot,state:RouterStateSnapshot){
		const token=localStorage.getItem('id_token');
		if(!token){
			return true;
		}
		var isStillValid=this.getExpiration(token);
		if(isStillValid){
			this.router.navigate(['/dashboard'])
		}else{
			return true;
		}

	}
}
