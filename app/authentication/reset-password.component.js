"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var angular2_notifications_1 = require('angular2-notifications');
var authentication_service_1 = require('../authentication/service/authentication.service');
var redirect_loggedin_guard_service_1 = require('../authentication/redirect-loggedin-guard.service');
var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(route, router, notif, auths, tokenAuth) {
        this.route = route;
        this.router = router;
        this.notif = notif;
        this.auths = auths;
        this.tokenAuth = tokenAuth;
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.route.queryParams.subscribe(function (resp) {
            if (!resp || !resp.token) {
                _this.notif.error('Error', 'Invalid Operation Performed');
                _this.router.navigate(['/login']);
                return;
            }
            _this.token = resp.token;
            var isValid = _this.tokenAuth.getExpiration(_this.token);
            if (!isValid) {
                _this.notif.error('Error', 'token has expired please try again');
                _this.router.navigate(['/login']);
                return;
            }
        }));
    };
    ResetPasswordComponent.prototype.changePassword = function () {
        var _this = this;
        if (this.password !== this.rePassword) {
            this.notif.error('Error', 'Both Passwords are Diffrent');
            return;
        }
        this.auths.changePassword({
            token: this.token,
            password: this.password
        }).subscribe(function (resp) {
            if (!resp) {
                _this.notif.error('Error', 'Some Error ');
            }
            if (resp && resp.code >= 600) {
                _this.notif.error('Error', resp.message);
            }
            else {
                _this.notif.success('Success', 'Changed Password Successfulyl');
            }
            _this.router.navigate(['/login']);
        });
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'reset-password',
            templateUrl: 'reset-password.component.html',
            styleUrls: ['forgot-password.component.css']
        }),
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, angular2_notifications_1.NotificationsService, authentication_service_1.AuthService, redirect_loggedin_guard_service_1.RedirectIfLoggedIn])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=reset-password.component.js.map