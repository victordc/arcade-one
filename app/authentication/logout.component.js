"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var angular2_notifications_1 = require('angular2-notifications');
var router_1 = require('@angular/router');
var LogoutComponent = (function () {
    function LogoutComponent(router, notif) {
        this.router = router;
        this.notif = notif;
    }
    LogoutComponent.prototype.ngOnInit = function () {
        this.logout();
    };
    LogoutComponent.prototype.logout = function () {
        localStorage.removeItem('id_token');
        this.router.navigate(['login']);
        this.notif.success('Successfully logged out', '');
    };
    LogoutComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'logout',
            template: ""
        }),
        core_2.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, angular2_notifications_1.NotificationsService])
    ], LogoutComponent);
    return LogoutComponent;
}());
exports.LogoutComponent = LogoutComponent;
//# sourceMappingURL=logout.component.js.map