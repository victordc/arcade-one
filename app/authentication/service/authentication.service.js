"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('@angular/http');
var secure_http_service_1 = require('../../secure-http.service');
var core_1 = require('@angular/core');
var AuthService = (function () {
    function AuthService(http, authHttp) {
        this.http = http;
        this.authHttp = authHttp;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
    }
    AuthService.prototype.login = function (user) {
        console.log(user);
        var userDetails = this.http
            .post(this.baseURL + "/login", {
            email: user.email,
            password: user.password,
            type: user.type || 'email',
            role: user.role || 'admin'
        }, this.getHeaders())
            .map(function (resp) { return resp.json(); });
        return userDetails;
    };
    AuthService.prototype.forgotPassword = function (data) {
        var confirm = this.http.post(this.baseURL + "/resetadminpassword", data).map(function (resp) { return resp.json(); });
        return confirm;
    };
    AuthService.prototype.changePassword = function (data) {
        console.log(data);
        var change = this.http.post(this.baseURL + "/changepassword", data).map(function (resp) { return resp.json(); });
        return change;
    };
    AuthService.prototype.getHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Accept', 'application/json');
        return headers;
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, secure_http_service_1.AuthHttp])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=authentication.service.js.map