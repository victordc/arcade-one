import { Http, Response, Headers, RequestOptions } from '@angular/http'
import {Observable} from 'rxjs/Rx';
import { AuthHttp } from '../../secure-http.service';

import {Injectable,Inject} from '@angular/core';
@Injectable()
export class AuthService {
	constructor(private http:Http,private authHttp:AuthHttp){}
	private baseURL='http://instatise-server-dev.herokuapp.com/api';
	login(user:any):Observable<any>{
		console.log(user);
		let userDetails=this.http
			.post(`${this.baseURL}/login`,{
				email : user.email,
				password : user.password,
				type : user.type || 'email',
				role : user.role || 'admin'
			},this.getHeaders())
			.map((resp:any)=>resp.json());

		return userDetails;
	}
	forgotPassword(data:any):Observable<any>{
		let confirm=this.http.post(`${this.baseURL}/resetadminpassword`,data).map((resp:any)=>resp.json());
		return confirm;
	}
	changePassword(data:any):Observable<any>{
		console.log(data);
		let change=this.http.post(`${this.baseURL}/changepassword`,data).map((resp:any)=>resp.json());
		return change;
	}
	private getHeaders(){
		let headers=new Headers();
		headers.append('Accept','application/json');
		return headers;
	}
}