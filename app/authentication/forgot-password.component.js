"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('./service/authentication.service');
var angular2_notifications_1 = require('angular2-notifications');
var router_1 = require('@angular/router');
var ForgotPasswordComponent = (function () {
    function ForgotPasswordComponent(auths, notif, router) {
        this.auths = auths;
        this.notif = notif;
        this.router = router;
    }
    ForgotPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        var hostname = location.href.split('/')[0] + '//' + location.href.split('//')[1].split('/')[0];
        this.auths.forgotPassword({
            email: this.email,
            domain: hostname
        }).subscribe(function (resp) {
            if (typeof resp === 'object' && resp.code === 611) {
                _this.notif.error('Error', resp.message);
            }
            else {
                _this.notif.success('Success', 'An e-mail with reset link has been sent to ' + _this.email);
                _this.router.navigate(['/login']);
            }
        }, function (error) {
            _this.notif.error('Error', 'Some Error Occurred');
        });
    };
    ForgotPasswordComponent.prototype.ngOnInit = function () { };
    ForgotPasswordComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'forgot-password',
            templateUrl: 'forgot-password.component.html',
            styleUrls: ['forgot-password.component.css']
        }),
        core_1.Injectable(), 
        __metadata('design:paramtypes', [authentication_service_1.AuthService, angular2_notifications_1.NotificationsService, router_1.Router])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());
exports.ForgotPasswordComponent = ForgotPasswordComponent;
//# sourceMappingURL=forgot-password.component.js.map