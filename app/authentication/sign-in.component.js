"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('./service/authentication.service');
var angular2_notifications_1 = require('angular2-notifications'); //import
var secure_http_service_1 = require('../secure-http.service');
var router_1 = require('@angular/router');
var SignInComponent = (function () {
    function SignInComponent(auth, notif, secureAuth, router) {
        this.auth = auth;
        this.notif = notif;
        this.secureAuth = secureAuth;
        this.router = router;
        this.clicker = 0;
        this.user = { email: '', password: '' };
        this.auth = auth;
        this.notif = notif;
        this.secureAuth = secureAuth;
        this.router = router;
    }
    SignInComponent.prototype.ngOnInit = function () { };
    SignInComponent.prototype.login = function () {
        var _this = this;
        if (this.clicker)
            return;
        this.clicker = 1;
        this.auth.login({
            email: this.user.email,
            password: this.user.password
        }).subscribe(function (r) {
            _this.clicker = 0;
            console.log(r);
            if (r.code === 602 || r.code === 606) {
                _this.notif.error('Invalid ', 'Username/Password'); //error
                return;
            }
            if (r.code === 601) {
                _this.notif.error('Invalid ', 'User not registered as an Admin'); //error
                return;
            }
            if (!r || !r.token)
                return;
            localStorage.setItem('id_token', r.token);
            localStorage.setItem('user_id', r.user_id);
            _this.router.navigate(['/dashboard']);
            // this.notif.success('Success','Welcome Ad');//sucess //heading //description
            //making secure call
        }, function (error) {
            _this.clicker = 0;
            _this.notif.error('Invalid ', 'Username/Password'); //error
        });
        //making secure api call
    };
    SignInComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sign-in',
            templateUrl: 'sign-in.component.html',
            styleUrls: ['sign-in.component.css'],
            providers: [authentication_service_1.AuthService]
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthService, angular2_notifications_1.NotificationsService, secure_http_service_1.AuthHttp, router_1.Router])
    ], SignInComponent);
    return SignInComponent;
}());
exports.SignInComponent = SignInComponent;
//# sourceMappingURL=sign-in.component.js.map