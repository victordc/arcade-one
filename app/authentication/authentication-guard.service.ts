import { Injectable } from '@angular/core';
import { RouterModule, Routes,Router,ActivatedRouteSnapshot,RouterStateSnapshot,CanActivate } from '@angular/router';

@Injectable()
export class AuthenticationGuard implements CanActivate{
	constructor(private router:Router){

	}
	canActivate(next:ActivatedRouteSnapshot,state:RouterStateSnapshot){
		const nextURL=next.url[0].path;
		const current=state.url;
		if(nextURL=='login' || nextURL=='logout')
			return true;
		const token=localStorage.getItem('id_token');
		if(!token){
			this.router.navigate(['/login']);
			return false;
		}else{
			return true;
		}

	}
}
