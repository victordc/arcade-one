import { Component, OnInit } from '@angular/core';

import {AuthService} from './service/authentication.service';
import { NotificationsService,SimpleNotificationsComponent } from 'angular2-notifications';//import
import { AuthHttp } from '../secure-http.service';
import {Router} from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'sign-in',
	templateUrl: 'sign-in.component.html',
	styleUrls:['sign-in.component.css'],
	providers:[AuthService]
})
export class SignInComponent implements OnInit {
	clicker:number= 0;
	constructor(private auth:AuthService,private notif:NotificationsService,private secureAuth:AuthHttp,private router:Router) {
		this.auth=auth;
		this.notif=notif;
		this.secureAuth=secureAuth;
		this.router=router;

	}
	user={email:'',password:''};
	ngOnInit() { }
	login(){
		if(this.clicker)
			return
		this.clicker =1;
		this.auth.login({
			email: this.user.email,
			password : this.user.password
		}).subscribe((r)=>{
			this.clicker=0;
			console.log(r);
			if(r.code === 602 || r.code === 606){
				this.notif.error('Invalid ','Username/Password');//error
				return;
			}
			if(r.code === 601){
				this.notif.error('Invalid ','User not registered as an Admin');//error
				return;
			}
			if(!r || !r.token)
				return
			localStorage.setItem('id_token',r.token);
			localStorage.setItem('user_id',r.user_id);
			this.router.navigate(['/dashboard'])
			// this.notif.success('Success','Welcome Ad');//sucess //heading //description
			//making secure call
		},(error)=>{
			this.clicker=0;
			this.notif.error('Invalid ','Username/Password');//error
		});
		//making secure api call

	}
}
