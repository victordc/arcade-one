import { Component, OnInit,Injectable } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';   
import { NotificationsService } from 'angular2-notifications';
import {AuthService} from '../authentication/service/authentication.service';
import { RedirectIfLoggedIn } from '../authentication/redirect-loggedin-guard.service';

@Component({
	moduleId: module.id,
	selector: 'reset-password',
	templateUrl: 'reset-password.component.html',
	styleUrls:['forgot-password.component.css']
})
@Injectable()
export class ResetPasswordComponent implements OnInit {
	constructor(private route:ActivatedRoute,private router:Router,private notif:NotificationsService,private auths:AuthService,private tokenAuth:RedirectIfLoggedIn) { }
	password:string;
	rePassword:string;
	token:string;
	ngOnInit() { 
		console.log(this.route.queryParams.subscribe((resp:any)=>{
			if(!resp || !resp.token){
				this.notif.error('Error','Invalid Operation Performed')
				this.router.navigate(['/login'])
				return
			}
			this.token=resp.token;
			const isValid=this.tokenAuth.getExpiration(this.token);
			if(!isValid){
				this.notif.error('Error','token has expired please try again')
				this.router.navigate(['/login']);
				return;
			}
		}));

	}
	changePassword(){
		if(this.password!==this.rePassword){
			this.notif.error('Error','Both Passwords are Diffrent');
			return
		}
		this.auths.changePassword({
			token:this.token,
			password :this.password
		}).subscribe((resp)=>{
			if(!resp){
				this.notif.error('Error','Some Error ');
			}
			if(resp && resp.code>=600){
				this.notif.error('Error',resp.message);
			}else{
				this.notif.success('Success','Changed Password Successfulyl');
			}
			this.router.navigate(['/login']);
		})	
	}

	
}