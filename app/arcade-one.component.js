"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_notifications_1 = require('angular2-notifications');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var ArcadeOneComponent = (function () {
    function ArcadeOneComponent(notif, location, router) {
        var _this = this;
        this.notif = notif;
        this.sideHide = 0;
        this.options = {
            position: ["top", "right"],
            timeOut: 5000,
            showProgressBar: false,
            pauseOnHover: false,
            clickToClose: true,
            maxLength: 100,
            lastOnBottom: true,
            preventDuplicates: true
        };
        this.location = location;
        router.events.subscribe(function (event) {
            var x = _this.location.path();
            if (x == '/login' || x == '/forgot' || x == '') {
                _this.sideHide = 1;
            }
            else if (/change-password/.test(x)) {
                _this.sideHide = 1;
            }
            else {
                _this.sideHide = 0;
            }
        });
    }
    ArcadeOneComponent.prototype.ngOnInit = function () {
        var x = this.location.path();
        if (x == '/login' || x == '/forgot' || x == '') {
            this.sideHide = 1;
        }
        else if (/change-password/.test(x)) {
            this.sideHide = 1;
        }
        else {
            this.sideHide = 0;
        }
        // console.log(this.sideHide,x.indexOf('/change-password'));
        //nav links
        // this.navLinks = ['Home','Campaigns','Brands','Influencers','Conflicts','Payments','Admin Users','Logout'];
        this.navLinks = [{ 'name': 'Home', 'route': 'dashboard' },
            { 'name': 'Campaigns', 'route': 'campaigns' },
            { 'name': 'Brands', 'route': 'brands' },
            { 'name': 'Influencers', 'route': 'influencers' },
            // {'link': 'Conflicts', 'route': 'conflicts'},
            { 'name': 'Payments', 'route': 'payments' },
            { 'name': 'Admin Users', 'route': 'users' },
            { 'name': 'Logout', 'route': 'logout' }];
    };
    ArcadeOneComponent.prototype.success = function (msg) {
        this.notif.success((msg && msg.title) || msg, (msg && msg.details), {
            timeOut: 5000,
            showProgressBar: true,
            pauseOnHover: false,
            clickToClose: false,
            maxLength: 10
        });
    };
    ArcadeOneComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            providers: [angular2_notifications_1.NotificationsService, common_1.Location, { provide: common_1.LocationStrategy, useClass: common_1.PathLocationStrategy }],
            templateUrl: 'layout.component.html'
        }), 
        __metadata('design:paramtypes', [angular2_notifications_1.NotificationsService, common_1.Location, router_1.Router])
    ], ArcadeOneComponent);
    return ArcadeOneComponent;
}());
exports.ArcadeOneComponent = ArcadeOneComponent;
//# sourceMappingURL=arcade-one.component.js.map