//property of dietcode
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import ANGULAR || BROWSER || ES6 || CUSTOM  modules here
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
//import App Components for routes
//---------
//Authentication components
//-------------------------------
var sign_in_component_1 = require('./authentication/sign-in.component');
var logout_component_1 = require('./authentication/logout.component');
var forgot_password_component_1 = require('./authentication/forgot-password.component');
var reset_password_component_1 = require('./authentication/reset-password.component');
//--------
//Dashboard Component
//-----------------------
var dashboard_component_1 = require('./dashboard/dashboard.component');
//------------
//Campaigns Components
//------------------------
var campaigns_component_1 = require('./campaigns/campaigns.component');
var campaign_view_component_1 = require('./campaigns/campaign-view.component');
//------------
//Brnads Components
//-----------------------
var brands_component_1 = require('./brands/brands.component');
var brand_view_component_1 = require('./brands/brand-view.component');
//--------------
//Influencers Components
//-------------------------
var influencers_component_1 = require('./influencers/influencers.component');
var influencer_view_component_1 = require('./influencers/influencer-view.component');
//--------
//Payments Components
//-----------------------
var payments_component_1 = require('./payments/payments.component');
var payment_view_component_1 = require('./payments/payment-view.component');
var configure_paypal_component_1 = require('./payments/configure-paypal.component');
//--------
//User Components
//-----------------------
var user_view_component_1 = require('./users/user-view.component');
var user_form_component_1 = require('./users/user-form.component');
var users_component_1 = require('./users/users.component');
var authentication_guard_service_1 = require('./authentication/authentication-guard.service');
var redirect_loggedin_guard_service_1 = require('./authentication/redirect-loggedin-guard.service');
var routes = [
    // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: '', component: sign_in_component_1.SignInComponent, canActivate: [redirect_loggedin_guard_service_1.RedirectIfLoggedIn] },
    { path: 'login', component: sign_in_component_1.SignInComponent, canActivate: [redirect_loggedin_guard_service_1.RedirectIfLoggedIn] },
    { path: 'logout', component: logout_component_1.LogoutComponent },
    { path: 'forgot', component: forgot_password_component_1.ForgotPasswordComponent },
    { path: 'change-password', component: reset_password_component_1.ResetPasswordComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'campaigns', component: campaigns_component_1.CampaignsComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'campaigns/:id', component: campaign_view_component_1.CampaignComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'brands', component: brands_component_1.BrandsComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'brands/:id', component: brand_view_component_1.BrandComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'influencers', component: influencers_component_1.InfluencersComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'influencers/:id', component: influencer_view_component_1.InfluencerComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'payments', component: payments_component_1.PaymentsComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'payments/:id', component: payment_view_component_1.PaymentComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'configure-paypal', component: configure_paypal_component_1.ConfigurePaypalComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'users', component: users_component_1.UsersComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'users/add', component: user_form_component_1.UserFormComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: 'users/:id', component: user_view_component_1.UserComponent, canActivate: [authentication_guard_service_1.AuthenticationGuard] },
    { path: '**', redirectTo: 'login' },
];
var ArcadeOneRoutingModule = (function () {
    function ArcadeOneRoutingModule() {
    }
    ArcadeOneRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule],
            providers: [authentication_guard_service_1.AuthenticationGuard, redirect_loggedin_guard_service_1.RedirectIfLoggedIn]
        }), 
        __metadata('design:paramtypes', [])
    ], ArcadeOneRoutingModule);
    return ArcadeOneRoutingModule;
}());
exports.ArcadeOneRoutingModule = ArcadeOneRoutingModule;
/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
//# sourceMappingURL=routes.js.map