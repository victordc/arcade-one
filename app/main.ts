import {platformBrowserDynamic}    from '@angular/platform-browser-dynamic';

import { ArcadeOneModule } from './arcade-one.module';

platformBrowserDynamic().bootstrapModule(ArcadeOneModule);


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
