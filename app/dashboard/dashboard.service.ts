import { Http, Response, Headers, RequestOptions } from '@angular/http'
import {Observable} from 'rxjs/Rx';
import { AuthHttp } from '../secure-http.service';

import {Injectable,Inject} from '@angular/core';
@Injectable()
export class DashboardService {
	
	constructor(private authHttp:AuthHttp){

	}

	private baseURL = 'http://instatise-server-dev.herokuapp.com/api';
	
	getDashData():Observable<any>{

		// let headers = new Headers({ 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImV4cCI6MTQ4Mzk0MDQyNH0.Kr1s239i_RC10nZaPdQvr3oT9hsHN2OwKVpJckp7SLc' });
		// let options = new RequestOptions({ headers: headers, method: "get" });

		return this.authHttp.get(`${this.baseURL}/dashdata?campaign_limit=3&campaign_page=1`)
				.map(resp=>resp.json())
	}
}