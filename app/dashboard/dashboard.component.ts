import { Component, OnInit } from '@angular/core';
import {DashboardService} from './dashboard.service';
      
@Component({
	moduleId: module.id,
	selector: 'dashboard',
	templateUrl: 'dashboard.component.html',
	providers : [DashboardService],
	styleUrls : ['dashboard.component.css']

})
export class DashboardComponent implements OnInit {

	// Declaration of dashboard stats variables

	// Dashboard Stats data loading status
	dashboardStatus: string = 'loading';
	
	// Dashboard Stats data returned from the API call
	dashboardData:any;
	
	// Campaign Stats
	totalCampaigns:number = 0;
	openCampaigns:number = 0;
	thisMonthCampaigns:number = 0;
	lastMonthCampaigns:number = 0;
	
	// Brand Stats
	totalBrands:number = 0;
	thisMonthBrands:number = 0;
	lastMonthBrands:number = 0;

	// Influencer Stats
	totalInfluencers:number = 0;
	thisMonthInfluencers:number = 0;
	lastMonthInfluencers:number = 0;
	
	// Top Five Performers
	topFiveBrands:Array<any> = [];
	topFiveInfluencers:Array<any> = [];
	topFiveCategories:Array<any> = [];

	//Influencers
	firstName:string;
	lastName:string;
	nameLength:number;
	influencerNames:Array<any> = [];

	// Latest Campaigns
	latestCampaigns:Array<any> = [];
	latestCampaignsModified:Array<any> = [];

	// Arrow Class
	campaignArrow:string = '';
	brandArrow:string = '';
	influencerArrow:string = '';

	// text success/danger class
	campaignText:string = '';
	brandText:string = '';
	influencerText:string = '';

	channelName:string = '';

	constructor(private dashboardService:DashboardService) { }

	ngOnInit() { 

    	// get Dashboard data by making a REST API call
    	this.getDataFromServer();		
	}

	convertDate = (date:any) => {
		var day = '',
			month = '',
			year = '',
			endDate = '';

		var dateObject = new Date();
		var modDate = '';

		dateObject = new Date(date);

		modDate = dateObject.toString();

		day = modDate.slice(8,10);
		month = modDate.slice(4,7);
		year = modDate.slice(13,15);
		
		endDate = `${day} ${month} '${year}`;

		return endDate;
	}

	// Handler function to fetch the dashboard stats	
	getDataFromServer (){
        this.dashboardService.getDashData()
            .subscribe(
                (data) => {

                	// Succesfully fetched data
                	if(data.code === 200) {
                		
                		this.dashboardData = data;
	                	
	                	// Console logging to check data sanctity
	                	// console.log(this.dashboardData);
	                	// console.log(this.dashboardData.campaigns.total);

	                	// Campaign Stats
	                	this.totalCampaigns = this.dashboardData.campaigns.total;
	                	this.openCampaigns = this.dashboardData.campaigns.open;
	                	this.thisMonthCampaigns = this.dashboardData.campaigns.this_month;
	                	this.lastMonthCampaigns = this.dashboardData.campaigns.last_month;

	                	// Brand Stats
						this.totalBrands = this.dashboardData.brands.total;
	                	this.thisMonthBrands = this.dashboardData.brands.this_month;
	                	this.lastMonthBrands = this.dashboardData.brands.last_month;

	                	// Influencer Stats
	                	this.totalInfluencers = this.dashboardData.influencers.total;
	                	this.thisMonthInfluencers = this.dashboardData.influencers.this_month;
	                	this.lastMonthInfluencers = this.dashboardData.influencers.last_month;

	                	// Top Five Performers
	                	this.topFiveBrands = this.dashboardData.brands.top_five;
	                	this.topFiveInfluencers = this.dashboardData.influencers.top_five;
	                	this.topFiveCategories = this.dashboardData.categories.top_five;

	                	// Latest Campaigns
	                	this.latestCampaigns = this.dashboardData.campaigns.latest;
	                	// console.log(this.latestCampaigns);

	                	this.latestCampaignsModified = this.latestCampaigns;
	                	// console.log(this.latestCampaignsModified);

	                	// Influencer data modification
	                	for(var i=0; i<this.topFiveInfluencers.length; i++) {
	                		
	                		(this.topFiveInfluencers[i].first_name === null) ? this.firstName = '' : this.firstName = this.topFiveInfluencers[i].first_name;
	                		(this.topFiveInfluencers[i].last_name === null) ? this.lastName = '' : this.lastName = this.topFiveInfluencers[i].last_name;

	                		this.nameLength = this.firstName.length + this.lastName.length;
	                		// console.log(this.nameLength);

	                		(this.nameLength > 20) ? this.influencerNames[i] = this.firstName : this.influencerNames[i] = `${this.firstName} ${this.lastName}`;
	                		// console.log(this.influencerNames[i]);
	                		
	                	}

	                	// Modified Latest Campaigns
	                	for(var i=0; i<this.latestCampaigns.length; i++) {
	                		
	                		this.latestCampaignsModified[i].end_date = this.convertDate(this.latestCampaignsModified[i].end_date);

	                		this.latestCampaignsModified[i].facebookVisible = 'hide-facebook';
                			this.latestCampaignsModified[i].twitterVisible = 'hide-twitter';
                			this.latestCampaignsModified[i].youtubeVisible = 'hide-youtube';
                			this.latestCampaignsModified[i].instagramVisible = 'hide-instagram';

                			if(this.latestCampaignsModified[i].brand_name === null) {
                				this.latestCampaignsModified[i].brand_name = 'N/A';
                			}
                			// console.log(this.latestCampaignsModified[i].brand_name);

	                		for(var j=0; j<this.latestCampaignsModified[i].channels.length; j++) {
	                			
	                			this.channelName = this.latestCampaignsModified[i].channels[j];
	                			
	                			switch (this.channelName) {
	                				case 'facebook': 
	                								this.latestCampaignsModified[i].facebookVisible = 'show-facebook';
	                								break;					
	                				case 'twitter': 
	                								this.latestCampaignsModified[i].twitterVisible = 'show-twitter';
	                								break;
	                				case 'youtube': 
	                								this.latestCampaignsModified[i].youtubeVisible = 'show-youtube';
	                								break;
	                				case 'instagram': 
	                								this.latestCampaignsModified[i].instagramVisible = 'show-instagram';
	                			}
	                		}
	                	}

	                	// Comparison of monthly Campaign stats
	                	if (this.thisMonthCampaigns > this.lastMonthCampaigns) {
	                		this.campaignArrow = `ion-ios-arrow-thin-up`;
	                		this.campaignText = `text-success`;
	                	}
	                	else if (this.thisMonthCampaigns < this.lastMonthCampaigns) {
	                		this.campaignArrow = `ion-ios-arrow-thin-down`;
	                		this.campaignText = `text-danger`;
	                	}

	                	// Comparison of monthly Brand stats
	                	if (this.thisMonthBrands > this.lastMonthBrands) {
	                		this.brandArrow = `ion-ios-arrow-thin-up`;
	                		this.brandText = `text-success`;
	                	}
	                	else if (this.thisMonthBrands < this.lastMonthBrands) {
	                		this.brandArrow = `ion-ios-arrow-thin-down`;
	                		this.brandText = `text-danger`;
	                	}

	                	// Comparison of monthly Influencer stats
	                	if (this.thisMonthInfluencers > this.lastMonthInfluencers) {
	                		this.influencerArrow = `ion-ios-arrow-thin-up`;
	                		this.influencerText = `text-success`;
	                	}
	                	else if (this.thisMonthInfluencers < this.lastMonthInfluencers) {
	                		this.influencerArrow = `ion-ios-arrow-thin-down`;
	                		this.influencerText = `text-danger`;
	                	}

	                	this.dashboardStatus = 'active';
                	}

                	// Error fetching data
                	else {
                		alert(`Error: ${data.code}`);
                	}

                }, // put the data returned from the server in our variables
                error => { this.dashboardStatus = 'error' }, // in case of failure show this message
                () => {} //run this code in all cases
            );
    }
	
}