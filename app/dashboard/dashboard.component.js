"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var dashboard_service_1 = require('./dashboard.service');
var DashboardComponent = (function () {
    function DashboardComponent(dashboardService) {
        this.dashboardService = dashboardService;
        // Declaration of dashboard stats variables
        // Dashboard Stats data loading status
        this.dashboardStatus = 'loading';
        // Campaign Stats
        this.totalCampaigns = 0;
        this.openCampaigns = 0;
        this.thisMonthCampaigns = 0;
        this.lastMonthCampaigns = 0;
        // Brand Stats
        this.totalBrands = 0;
        this.thisMonthBrands = 0;
        this.lastMonthBrands = 0;
        // Influencer Stats
        this.totalInfluencers = 0;
        this.thisMonthInfluencers = 0;
        this.lastMonthInfluencers = 0;
        // Top Five Performers
        this.topFiveBrands = [];
        this.topFiveInfluencers = [];
        this.topFiveCategories = [];
        this.influencerNames = [];
        // Latest Campaigns
        this.latestCampaigns = [];
        this.latestCampaignsModified = [];
        // Arrow Class
        this.campaignArrow = '';
        this.brandArrow = '';
        this.influencerArrow = '';
        // text success/danger class
        this.campaignText = '';
        this.brandText = '';
        this.influencerText = '';
        this.channelName = '';
        this.convertDate = function (date) {
            var day = '', month = '', year = '', endDate = '';
            var dateObject = new Date();
            var modDate = '';
            dateObject = new Date(date);
            modDate = dateObject.toString();
            day = modDate.slice(8, 10);
            month = modDate.slice(4, 7);
            year = modDate.slice(13, 15);
            endDate = day + " " + month + " '" + year;
            return endDate;
        };
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // get Dashboard data by making a REST API call
        this.getDataFromServer();
    };
    // Handler function to fetch the dashboard stats	
    DashboardComponent.prototype.getDataFromServer = function () {
        var _this = this;
        this.dashboardService.getDashData()
            .subscribe(function (data) {
            // Succesfully fetched data
            if (data.code === 200) {
                _this.dashboardData = data;
                // Console logging to check data sanctity
                // console.log(this.dashboardData);
                // console.log(this.dashboardData.campaigns.total);
                // Campaign Stats
                _this.totalCampaigns = _this.dashboardData.campaigns.total;
                _this.openCampaigns = _this.dashboardData.campaigns.open;
                _this.thisMonthCampaigns = _this.dashboardData.campaigns.this_month;
                _this.lastMonthCampaigns = _this.dashboardData.campaigns.last_month;
                // Brand Stats
                _this.totalBrands = _this.dashboardData.brands.total;
                _this.thisMonthBrands = _this.dashboardData.brands.this_month;
                _this.lastMonthBrands = _this.dashboardData.brands.last_month;
                // Influencer Stats
                _this.totalInfluencers = _this.dashboardData.influencers.total;
                _this.thisMonthInfluencers = _this.dashboardData.influencers.this_month;
                _this.lastMonthInfluencers = _this.dashboardData.influencers.last_month;
                // Top Five Performers
                _this.topFiveBrands = _this.dashboardData.brands.top_five;
                _this.topFiveInfluencers = _this.dashboardData.influencers.top_five;
                _this.topFiveCategories = _this.dashboardData.categories.top_five;
                // Latest Campaigns
                _this.latestCampaigns = _this.dashboardData.campaigns.latest;
                // console.log(this.latestCampaigns);
                _this.latestCampaignsModified = _this.latestCampaigns;
                // console.log(this.latestCampaignsModified);
                // Influencer data modification
                for (var i = 0; i < _this.topFiveInfluencers.length; i++) {
                    (_this.topFiveInfluencers[i].first_name === null) ? _this.firstName = '' : _this.firstName = _this.topFiveInfluencers[i].first_name;
                    (_this.topFiveInfluencers[i].last_name === null) ? _this.lastName = '' : _this.lastName = _this.topFiveInfluencers[i].last_name;
                    _this.nameLength = _this.firstName.length + _this.lastName.length;
                    // console.log(this.nameLength);
                    (_this.nameLength > 20) ? _this.influencerNames[i] = _this.firstName : _this.influencerNames[i] = _this.firstName + " " + _this.lastName;
                }
                // Modified Latest Campaigns
                for (var i = 0; i < _this.latestCampaigns.length; i++) {
                    _this.latestCampaignsModified[i].end_date = _this.convertDate(_this.latestCampaignsModified[i].end_date);
                    _this.latestCampaignsModified[i].facebookVisible = 'hide-facebook';
                    _this.latestCampaignsModified[i].twitterVisible = 'hide-twitter';
                    _this.latestCampaignsModified[i].youtubeVisible = 'hide-youtube';
                    _this.latestCampaignsModified[i].instagramVisible = 'hide-instagram';
                    if (_this.latestCampaignsModified[i].brand_name === null) {
                        _this.latestCampaignsModified[i].brand_name = 'N/A';
                    }
                    // console.log(this.latestCampaignsModified[i].brand_name);
                    for (var j = 0; j < _this.latestCampaignsModified[i].channels.length; j++) {
                        _this.channelName = _this.latestCampaignsModified[i].channels[j];
                        switch (_this.channelName) {
                            case 'facebook':
                                _this.latestCampaignsModified[i].facebookVisible = 'show-facebook';
                                break;
                            case 'twitter':
                                _this.latestCampaignsModified[i].twitterVisible = 'show-twitter';
                                break;
                            case 'youtube':
                                _this.latestCampaignsModified[i].youtubeVisible = 'show-youtube';
                                break;
                            case 'instagram':
                                _this.latestCampaignsModified[i].instagramVisible = 'show-instagram';
                        }
                    }
                }
                // Comparison of monthly Campaign stats
                if (_this.thisMonthCampaigns > _this.lastMonthCampaigns) {
                    _this.campaignArrow = "ion-ios-arrow-thin-up";
                    _this.campaignText = "text-success";
                }
                else if (_this.thisMonthCampaigns < _this.lastMonthCampaigns) {
                    _this.campaignArrow = "ion-ios-arrow-thin-down";
                    _this.campaignText = "text-danger";
                }
                // Comparison of monthly Brand stats
                if (_this.thisMonthBrands > _this.lastMonthBrands) {
                    _this.brandArrow = "ion-ios-arrow-thin-up";
                    _this.brandText = "text-success";
                }
                else if (_this.thisMonthBrands < _this.lastMonthBrands) {
                    _this.brandArrow = "ion-ios-arrow-thin-down";
                    _this.brandText = "text-danger";
                }
                // Comparison of monthly Influencer stats
                if (_this.thisMonthInfluencers > _this.lastMonthInfluencers) {
                    _this.influencerArrow = "ion-ios-arrow-thin-up";
                    _this.influencerText = "text-success";
                }
                else if (_this.thisMonthInfluencers < _this.lastMonthInfluencers) {
                    _this.influencerArrow = "ion-ios-arrow-thin-down";
                    _this.influencerText = "text-danger";
                }
                _this.dashboardStatus = 'active';
            }
            else {
                alert("Error: " + data.code);
            }
        }, // put the data returned from the server in our variables
        function (// put the data returned from the server in our variables
            error) { _this.dashboardStatus = 'error'; }, // in case of failure show this message
        function () { } //run this code in all cases
        );
    };
    DashboardComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'dashboard',
            templateUrl: 'dashboard.component.html',
            providers: [dashboard_service_1.DashboardService],
            styleUrls: ['dashboard.component.css']
        }), 
        __metadata('design:paramtypes', [dashboard_service_1.DashboardService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map