"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var secure_http_service_1 = require('../secure-http.service');
var core_1 = require('@angular/core');
var DashboardService = (function () {
    function DashboardService(authHttp) {
        this.authHttp = authHttp;
        this.baseURL = 'http://instatise-server-dev.herokuapp.com/api';
    }
    DashboardService.prototype.getDashData = function () {
        // let headers = new Headers({ 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImV4cCI6MTQ4Mzk0MDQyNH0.Kr1s239i_RC10nZaPdQvr3oT9hsHN2OwKVpJckp7SLc' });
        // let options = new RequestOptions({ headers: headers, method: "get" });
        return this.authHttp.get(this.baseURL + "/dashdata?campaign_limit=3&campaign_page=1")
            .map(function (resp) { return resp.json(); });
    };
    DashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [secure_http_service_1.AuthHttp])
    ], DashboardService);
    return DashboardService;
}());
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.service.js.map