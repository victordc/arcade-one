import './rxjs-extensions';

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule }    from '@angular/http';
import { MaterialModule } from '@angular/material';
import { ArcadeOneRoutingModule } from './routes';
import { Ng2SelectModule } from 'ng2-material-select';

//import App Components for routes
import { ArcadeOneComponent }  from './arcade-one.component';
//---------
//Authentication components
//-------------------------------
import { SignInComponent }  from './authentication/sign-in.component';
import { LogoutComponent }  from './authentication/logout.component';
import { ForgotPasswordComponent }  from './authentication/forgot-password.component';
import { ResetPasswordComponent }  from './authentication/reset-password.component';
//--------
//Dashboard Component
//-----------------------
import { DashboardComponent }  from './dashboard/dashboard.component';
//------------
//Campaigns Components
//------------------------
import { CampaignsComponent }  from './campaigns/campaigns.component';
import { CampaignComponent }  from './campaigns/campaign-view.component';
//------------
//Brnads Components
//-----------------------
import { BrandsComponent }  from './brands/brands.component';
import { BrandComponent }  from './brands/brand-view.component';
//--------------
//Influencers Components
//-------------------------
import { InfluencersComponent }  from './influencers/influencers.component';
import { InfluencerComponent }  from './influencers/influencer-view.component';
//--------
//Payments Components
//-----------------------
import { PaymentsComponent }  from './payments/payments.component';
import { PaymentComponent }   from './payments/payment-view.component';
import { ConfigurePaypalComponent } from './payments/configure-paypal.component';

//--------
//User Components
//-----------------------
import { UsersComponent }  from './users/users.component';
import { UserComponent }   from './users/user-view.component';
import { UserFormComponent }  from './users/user-form.component';

//--------
//User Components
//-----------------------
import {AuthService} from './authentication/service/authentication.service';
import {SimpleNotificationsModule,SimpleNotificationsComponent} from 'angular2-notifications';
import { AuthenticationGuard } from './authentication/authentication-guard.service';
import { AUTH_PROVIDERS } from './secure-http.service';

//----
// pipes
//----------
import { ExponentialStrengthPipe } from './helpers/readablenumber-pipe';
import { CapitalizePipe } from './helpers/capitalize.pipe';
import { TruncatePipe }   from './helpers/ellipsis.pipe';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    ArcadeOneRoutingModule,
    SimpleNotificationsModule,
    MaterialModule.forRoot(),
    Ng2SelectModule

  ],
  declarations: [
    ArcadeOneComponent,
    SignInComponent,
    LogoutComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    DashboardComponent,
    CampaignsComponent,
    CampaignComponent,
    BrandsComponent,
    BrandComponent,
    InfluencersComponent,
    InfluencerComponent,
    PaymentsComponent,
    PaymentComponent,
    ConfigurePaypalComponent,
    UsersComponent,
    UserComponent,
    UserFormComponent,
    ExponentialStrengthPipe,
    CapitalizePipe,
    TruncatePipe
  ],
  providers: [
    AuthService,AuthenticationGuard,AUTH_PROVIDERS
    ],//pass services here
    bootstrap: [ ArcadeOneComponent ]
})
export class ArcadeOneModule { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
